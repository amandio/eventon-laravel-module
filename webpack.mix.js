const mix = require('laravel-mix');

// mix.js('src/assets/js/app.js', 'src/public/js')
   mix.js('src/assets/js/admin.js', 'src/public/js');
   // .extract(['vue', 'accounting', 'moment', 'axios']);

// mix.sass('src/assets/scss/main.scss', 'src/public/css')
   mix.sass('src/assets/scss/admin.scss', 'src/public/css');

if (mix.inProduction()) {
  mix.options({
    processCssUrls: false,
    // extractVueStyles: 'src/public/css/components.css'
  })
      .version();
}
