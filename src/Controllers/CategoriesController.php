<?php
/**
 * Created by PhpStorm.
 * User: anhamande
 * Date: 2019-06-11
 * Time: 00:01
 */

namespace CodeonWeekends\Eventon\Controllers;


use CodeonWeekends\Eventon\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends BaseController
{

    public function index(Request $request)
    {
        $categories = Category::query();
        $info = [
            'total' => $categories->count()
        ];

        /**
         * Prepare query results order based on client request
         */
        if ($request->has('orderby') || $request->has('ob')) {
            $categories = $categories->orderBy($request->orderby ?: $request->ob);
        }

        if ($request->has('sort') || $request->has('s')) {
            switch ($request->sort ?: $request->s) {
                case 'random' || 'r':
                    $categories = $categories->inRandomOrder();
                    break;

                case 'asc' || 'a':
                    $categories = $categories->orderBy('id', 'asc');
                    break;

                case 'desc' || 'd':
                    $categories = $categories->orderByDesc('id');
                    break;
            }
        }

        if ($request->has('limit') || $request->has('l')) {
            $categories = $categories->limit($request->limit ?: $request->l);
        }

        return response()->json([
            'categories' => $categories->get(),
            'info' => $info
        ]);
    }
}