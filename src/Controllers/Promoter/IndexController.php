<?php

namespace CodeonWeekends\Eventon\Controllers\Promoter;

use Illuminate\Http\Request;
use CodeonWeekends\Eventon\Controllers\BaseController;
use CodeonWeekends\Eventon\Models\Promoter;

class IndexController extends BaseController
{
    public function index()
    {
        return view('eventon::admin.index');
    }

    public function getBookedEvents(Request $request)
    {
        return Promoter::find($request->id)->events()->where('status', 1)->get();
    }

    public function getEvents(Request $request)
    {
        return Promoter::find($request->id)->events;
    }
}