<?php

namespace CodeonWeekends\Eventon\Controllers;

use App\Events\BookingComplete;
use App\Models\Transaction;
use Carbon\Carbon;
use CodeonWeekends\Eventon\Models\Booking;
use CodeonWeekends\Eventon\Models\Category;
use CodeonWeekends\Eventon\Models\Event;
use CodeonWeekends\Eventon\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EventsController extends BaseController
{

    public function index()
    {
        return view('eventon::index');
    }

    /**
     * Display paginated events in random order
     *
     * @param Request $request
     * @return Event[]|\Illuminate\Contracts\View\Factory|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function events(Request $request)
    {
        $query = (new Event())
            ->future()
            ->available();

        if ($request->has('city') && $request->city != 'all') {
            $query->city($request->city);
        }

        $query->OrderBy('id', 'desc')
              ->with('tickets');

        $events = $this->paginateResults($query, 12, $this->page);

        if ($request->ajax() || $request->wantsJson()) {
            return response()->json($events);
        }
        return view('eventon::events', compact('events'));
    }

    /**
     * Display events by category
     *
     * @param Category $category
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws NotFoundHttpException
     */
    public function byCategory(Category $category, Request $request)
    {
        $query = $category->events()
            ->future()
            ->available();

        if ($request->has('city') && $request->city != 'all') {
            $query->city($request->city);
        }

        $query->OrderBy('id', 'desc')->with('tickets');

        $events = $this->paginateResults($query, 12, $this->page);

        if ($request->ajax() || $request->wantsJson()) {
            return response()->json($events);
        }
        return view('eventon::events', compact('events'));
    }

    /**
     * View a single event
     *
     * @param Event $event
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function view(Event $event, Request $request)
    {
        if ($event->is_available) {
            $title = $event->title;
            $event->load('tickets');

            if ($request->ajax() || $request->wantsJson()) {
                return response()->json($event);
            }
            return view('eventon::single_event', compact('event', 'title'));
        } else {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json([
                    'message' => 'O Evento encontra-se indisponivel.'
                ], 404);
            }
            abort(404);
        }
    }
}