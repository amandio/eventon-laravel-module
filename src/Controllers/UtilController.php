<?php

namespace CodeonWeekends\Eventon\Controllers;

use Illuminate\Http\Request;
use Milon\Barcode\DNS2D;

class UtilController extends BaseController
{
    public function generateQrCode(Request $request)
    {
        return DNS2D::getBarcodeSVG($request->get('content'), 'QRCODE,Q', $request->width ?? 2, $request->height ?? 2);
    }
}
