<?php

namespace CodeonWeekends\Eventon\Controllers;

use CodeonWeekends\Eventon\Models\Event;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class BaseController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected const RESULTS_LIMIT = 20;
    protected $page = 1;

    public function __construct(Request $request)
    {
        $this->page = $request->has('p') ? $request->p : 1;
    }

    protected function paginateResults($model, $limit = self::RESULTS_LIMIT, $page = 1)
    {
        $page--;
        $offset = $limit * $page;

        return $model->limit($limit)->offset($offset)->get();
    }

    public function help()
    {
        return view('eventon::help');
    }

    public function about()
    {
        return view('eventon::about');
    }

    public function contactus()
    {
        return view('eventon::contactus');
    }

    public function faq()
    {
        return view('eventon::faq');
    }

    public function terms()
    {
        return view('eventon::terms');
    }
}