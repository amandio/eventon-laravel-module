<?php
/**
 * @copyright 2020.
 * @author Amandio Khuta Nhamande <amandio16@gmail.com>
 * @license MIT
 *
 */

/**
 * Created by PhpStorm.
 * User: anhamande
 * Date: 2019-06-11
 * Time: 00:01
 */

namespace CodeonWeekends\Eventon\Controllers\Admin;

use CodeonWeekends\Eventon\Controllers\CategoriesController as BaseCategoriesController;
use CodeonWeekends\Eventon\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class CategoriesController extends BaseCategoriesController
{

    public function index(Request $request)
    {
        return parent::index($request);
    }

    public function store(Request $request)
    {
        $this->validator($this->getData($request))->validate();

        if ($category = Category::create($this->getData($request))) {
            return response()->json([
                'code' => 'B00',
                'message' => __('Categoria criada com sucesso.'),
                'category' => $category
            ]);
        } else {
            return response()->json([
                'code' => 'B209',
                'message' => __('Falha ao criar categoria.')
            ]);
        }
    }

    public function view()
    {}

    public function destroy()
    {}

    public function update()
    {}

    protected function getData(Request $request)
    {
        $alias = Str::slug($request->name);

        return [
            'name' => $request->name,
            'alias' => $alias
        ];
    }

    protected function validator($data)
    {
        return Validator::make($data, [
            'name' => 'required|alpha',
            'alias' => 'unique:eventon_categories,alias'
        ]);
    }
}