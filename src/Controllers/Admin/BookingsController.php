<?php

namespace CodeonWeekends\Eventon\Controllers\Admin;

use Carbon\Carbon;
use CodeonWeekends\Eventon\Controllers\BaseController;
use CodeonWeekends\Eventon\Models\Booking;
use CodeonWeekends\Eventon\Models\Ticket;
use CodeonWeekends\Eventon\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Milon\Barcode\DNS2D;
use PHPUnit\Runner\Exception;
use CodeonWeekends\Eventon\Controllers\BookingsController as BaseBookingsController;

class BookingsController extends BaseBookingsController
{
    public function index(Request $request)
    {
        $bookings = Booking::with('event')->get();

        if($request->has('load')) {
            $bookings->load(explode(',', $request->load));
        }

        $info = [
            'total' => $bookings->count(),
            'confirmed' => Booking::confirmed()->count(),
            'pending' => Booking::unconfirmed()->count(),
            'cancelled' => Booking::cancelled()->count()
        ];

        if ($request->ajax() || $request->wantsJson()) {
            return response()->json([
                'bookings' => $bookings,
                'info' => $info
            ]);
        }

        return view('eventon::admin.bookings.index', compact($bookings));
    }

    public function view (Request $request)
    {
        try {
            $booking = Booking::findOrFail($request->number);

            if ($request->has('load')) {
                $booking->load(explode(',', $request->load));
            }

            if ($request->ajax() || $request->wantsJson()) {
                return response()->json($booking);
            }

            return view('eventon::admin.bookings.view', compact($booking));
        } catch(\Exception $e) {
            return response([
                'message' => __('Não foi possivel encontrar a reserva.'),
                'code' => $e->getCode()
            ], 404);
        }
    }

    public function printPreview (Booking $booking, bool $print = false)
    {
        $content = 'type:print,date:' . Carbon::now() . ',number:'. $booking->number;
        $encoded = base64_encode($content);

        $qrcode = DNS2D::getBarcodeSVG($encoded, 'QRCODE,H', 2, 2);

        return view('eventon::admin.bookings.ticket_pdf', compact('booking', 'qrcode', 'print'));
    }

    public function print (Booking $booking)
    {
        return $this->pdf($booking, true);
    }

    public function download (Booking $booking)
    {
        return $this->pdf($booking);
    }

    public function pdf(Booking $booking, $print = false)
    {
        $pdf = App::make('dompdf.wrapper');

        $content = 'type:print,date:' . Carbon::now() . ',number:'. $booking->number;
        $encoded = base64_encode($content);

        $qrcode = DNS2D::getBarcodeHTML($encoded, 'QRCODE,Q', 2, 2);
        $result = $pdf->loadView('eventon::admin.bookings.ticket_pdf', compact('booking', 'qrcode', 'print'))
            ->setPaper('A5', 'landscape');

        return $print ? $result->stream() : $result->download($booking->number . '.pdf');
    }

    public function store (Request $request)
    {
        $this->validate($request, [
            'event_id' => 'required|integer:exists:'. config('eventon.database.table_prefix') . 'events,id',
            'ticket_id' => 'required|integer|exists:'. config('eventon.database.table_prefix') . 'tickets,id',
            'contact_full_name' => 'required',
            'contact_phone' => 'required|numeric',
            'contact_email' => 'required|email',
            'contact_document_number' => 'required',
            'payment_method' => 'required',
            'allowed_entrances' => 'required|numeric|min:1|max:' . Ticket::find($request->ticket_id)->first()->max_reservation_quantity
        ]);

        $ticket = Ticket::findOrFail($request->ticket_id);

        // Check if requested entrance quantity is available so that can be booked
        // if available decrease the ticket quantity and proceed to booking
        if ($this->checkAndDecreaseTicketQuantity($ticket, $request->allowed_entrances)) {
            try {
                $data = $request->all();
                $data['user_id'] = Auth::user()->id;
                $data['transaction_id'] = Transaction::create([
                    'type' => 'payment',
                    'host_ip' => $request->ip(),
                    'host_os' => '',
                    'host_user_agent' => $request->userAgent(),
                    'data' => '',
                    'amount' => ($request->allowed_entrances * $ticket->price) * 1.17,
                    'source' => $request->payment_method,
                    'description' => 'Booking reservation.',
                    'reference' => Str::random(12),
                    'code' => rand(1000, 9999),
                    'status' => 1,
                ])->id;
                $data['status'] = 1;
                $data['print_on_site'] = 1;
                $data['printed_at'] = Carbon::now();
                $data['print_count'] = 1;
                $data['number'] = $this->generateTicketNumber($request->allowed_entrances);

                $booking = new Booking($data);

                if ($booking->save()) {
                    return [
                        'message' => __('A reserva foi efectuada com sucesso.'),
                        'booking' => $booking
                    ];
                }
            } catch (Exception $e) {
                return response([
                    'code' => $e->getCode(),
                    'message' => __('Falha ao efectuar a reserva. Tente novamente mais tarde.')
                ], 403);
            }
        } else {
            return response([
                'message' => __('O bilhete escolhido está indisponivel no momento.')
            ],422);
        }
    }

    public function update (Request $request)
    {}

    public function destroy (Request $request)
    {
        $table = Config::get('eventon.database.table_prefix') . 'bookings';

        $this->validate($request, [
            'number' => 'required|exists:' . $table . ',number'
        ]);

        $booking = Booking::findOrFail($request->number);

        if ($booking->delete()) {
            $response = [
                'code' => 'B00',
                'message' => 'Reserva eliminada com sucesso.'
            ];

            if ($request->ajax() || $request->wantsJson()) {
                return response()->json($response);
            }

            return redirect()->back()->with($response);
        }

        $response = [
            'code' => 'B12',
            'message' => 'Falha ao eliminar reserva'
        ];

        if ($request->ajax() || $request->wantsJson()) {
            return response()->json($response, 400);
        }

        return redirect()->back(400)->with($response);
    }
}
