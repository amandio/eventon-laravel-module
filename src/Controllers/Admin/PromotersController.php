<?php

namespace Codeonweekends\Eventon\Controllers\Admin;

use CodeonWeekends\Eventon\Controllers\BaseController;
use CodeonWeekends\Eventon\Models\Promoter;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PromotersController extends BaseController
{
    public function index (Request $request)
    {
        $promoters = Promoter::all();
        $info = [
            'total' => $promoters->count()
        ];

        if ($request->ajax() || $request->wantsJson()) {
            return response()->json([
                'promoters' => $promoters,
                'info' => $info
            ]);
        }

        abort(404);
    }

    public function store (Request $request)
    {
        $this->validation($request);
        $this->validate($request, [
            'email' => 'required|email|unique:' . config('eventon.database.table_prefix') . 'promoters,email',
            'representative_email' => 'required|email|unique:' . config('eventon.database.table_prefix') . 'promoters,representative_email'
        ]);

        $data = $request->all();

        $data['phone_alt'] = $request->filled('phone_alt') ? $request->phone_alt : 0;
        $data['representative_phone_alt'] = $request->filled('representative_phone_alt') ? $request->representative_phone_alt : 0;
        $data['alias'] = Str::slug($request->name);

        // Temporary bypass for branding
        $data['branding'] = Str::random(5);

        $promoter = new Promoter($data);

        if ($promoter->save()) {
            // TODO: Create user and profile for promoter

            return response()->json([
                'code' => 'B00',
                'message' => __('Promotor criado com sucesso.'),
                'promoter' => $promoter
            ]);
        } else {
            return response()->json([
                'code' => '',
                'message' => __('Falha ao criar promotor.')
            ]);
        }
    }

    public function view (Promoter $promoter)
    {
        return $promoter;
    }

    public function update (Request $request)
    {
        $this->validation($request);
        $this->validate($request, [
            'id' => 'required|exists:eventon_promoters,id'
//            'email' => 'required|email|unique:eventon_promoters,email',
//            'representative_email' => 'required|email|unique:eventon_promoters,email'
        ]);

        $promoter = Promoter::findOrFail($request->id);

        if ($promoter->update($request->all())) {
            return response()->json([
                'code' => 'B00',
                'message' => 'Dados atualizados com sucesso.',
                'promoter' => $promoter
            ]);
        } else {
            return response()->json([
                'code' => '',
                'message' => ''
            ]);
        }
    }

    public function destroy (Request $request)
    {}

    protected function validation(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'address_city' => 'required',
            'address_province' => 'required',
            'representative_name' => 'required',
            'representative_phone' => 'required',
            'representative_address' => 'required',
            'representative_address_city' => 'required',
            'representative_address_province' => 'required',
        ]);
    }
}