<?php

namespace CodeonWeekends\Eventon\Controllers\Admin;

use App\Http\Controllers\Util\FilesController;
use CodeonWeekends\Eventon\Controllers\BaseController;
use CodeonWeekends\Eventon\Models\Event;
use Illuminate\Http\Request;

class EventsController extends BaseController
{

    public function index(Request $request)
    {
        $queryString = $request->has('qs') ? $request->qs : null;
        $events = [];

        if ($queryString) {
            $events = Event::where([
                ['title', 'like', '%' . $queryString .'%']
            ])->get();
        } else {
            $events = Event::confirmed()->get();

            if ($request->has('fetch_all') && $request->fetch_all == 1) {
                $events = Event::all();
                $events->load('promoter');

                $info = [
                    'total' => Event::count(),
                    'confirmed' => Event::confirmed()->count(),
                    'pending' => Event::pending()->count()
                ];
            }
        }

        if ($request->has('load') && $request->load) {
            $events->load(explode(',', $request->load));
        }

        if ($request->ajax() || $request->wantsJson()) {
            return response()->json([
                'events' => $events,
                'info' => isset($info) ? $info : null
            ]);
        }

        return view('eventon::admin.events.index', compact($events));
    }

    public function view(Request $request)
    {
        $event = Event::findOrFail($request->id);
        $event->load('tickets.bookings', 'promoter');

        return $event;
    }

    public function store(Request $request)
    {
        $prefix = config('eventon.database.table_prefix');

        $this->validate($request, [
            'event.title' => 'required|max:255',
            'event.description' => 'required',
            'event.cover' => 'required',
            'event.location_city' => 'required',
            'event.location' => 'required',
            'event.promoter_id' => 'required|exists:'. $prefix . 'promoters,id',
            'event.begin_time' => 'required|date_format:d/m/Y H:i',
            'event.end_time' => 'required|date_format:d/m/Y H:i',
            'event.category_id' => 'required|integer|exists:' . $prefix . 'categories,id'
        ]);

        // TODO: Move temporary uploaded images
        $event = $request->event;
        $file = new FilesController();

        $event['cover'] = $file->move($event['cover'], 'public/events');
        $event['cover_thumbnail'] = $file->move($event['cover_thumbnail'], 'public/events');

        try {
            $event = new Event($event);

            if ($event->save()) {
                $event->categories()->attach($request->event['category_id']);

                return response()->json([
                    'code' => 'B00',
                    'message' => __('Evento agendado com sucesso.'),
                    'event' => $event
                ]);
            } else {
                return response()->json([
                    'code' => 'B09',
                    'message' => __('Falha ao agendar evento.')
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'log' => $e->getTrace()
            ]);
        }
    }

    public function update(Request $request)
    {
        dd($request->all());
    }

    public function destroy(Request $request)
    {
        $event = Event::findOrFail($request->id);

        try {
            if ($event->delete()) {
                return response()->json([
                    'code' => 'B00',
                    'message' => __('Evento eliminado com sucesso.')
                ]);
            } else {
                return response()->json([
                    'code' => 'B11',
                    'message' => __('Falha ao eliminar evento.')
                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json($e);
        }
    }

    public function tickets(Request $request)
    {
        $event = Event::find($request->id);
        $event->load('tickets.bookings');

        return $event->tickets;
    }

    public function bookings(Request $request)
    {
        $event = Event::findOrFail($request->id);

        if ($request->has('load')){
            $event->load(explode(',', $request->load));
        }

        return [
            'bookings' => $event->bookings,
            'info' => [
                'event_title' => $event->title,
                'confirmed' => $event->bookings()->confirmed()->count(),
                'pending' => $event->bookings()->pending()->count(),
                'total' => $event->bookings()->count()
            ]
        ];
    }

    public function publish(Request $request)
    {
        try {
            $event = Event::findOrFail($request->id);
            $event->update(['status' => $request->status]);

            return [
                'message' => __('O estado do evento foi alterado com sucesso.'),
                'event' => $event
            ];
        } catch (\Exception $e) {
            return response([
                'message' => __('O evento não foi encontrado.'),
                'code' => $e->getCode()
            ], 404);
        }
    }
}
