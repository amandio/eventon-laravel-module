<?php

namespace CodeonWeekends\Eventon\Controllers\Admin;

use CodeonWeekends\Eventon\Controllers\BaseController;

class IndexController extends BaseController
{
    public function index()
    {
        return view('eventon::admin.index');
    }
}