<?php namespace Codeonweekends\Eventon\Controllers\Admin;

use CodeonWeekends\Eventon\Controllers\BaseController;
use CodeonWeekends\Eventon\Models\Event;
use CodeonWeekends\Eventon\Models\Ticket;
use Illuminate\Http\Request;

class TicketsController extends BaseController
{
    public function all()
    {
        $tickets = Ticket::all();
        return [
            'tickets' => $tickets,
            'info' => [
                'total' => $tickets->count(),
                'published' => Ticket::published()->count(),
                'unpublished' => Ticket::unpublished()->count()
            ]
        ];
    }

    public function view(Request $request)
    {
        try {
            $ticket = Ticket::findOrFail($request->id);
            $ticket->load('bookings');

            return $ticket;
        } catch (\Exception $e) {
            return response([
                'message' => __('Nenhum resultado encontrado para a pesquisa.'),
                'code' => $e->getCode()
            ], 404);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'ticket.name' => 'required',
            'ticket.description' => 'required',
            'ticket.quantity' => 'required|integer',
            'ticket.min_reservation_quantity' => 'required|integer',
            'ticket.max_reservation_quantity' => 'required|integer',
            'ticket.price' => 'required|integer'
        ]);

        $event = Event::findOrFail($request->event_id);
        $ticket = new Ticket($request->ticket);

        try {
            if ($ticket->save()) {
                $event->tickets()->attach($ticket);

                return response()->json([
                    'code' => 'B00',
                    'message' => __('Bilhete/Entrada adicionada com sucesso.')
                ]);
            } else {
                return response()->json([
                    'code' => 'B10',
                    'message' => __('Falha ao adicionar Bilhete/Entrada.')
                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'trace' => $e->getTrace()
            ]);
        }
    }

    public function update(Request $request)
    {
        $ticket = Ticket::find($request->id);

        if ($ticket->update($request->all())) {
            return [
                'message' => __('Bilhete atualizado com sucesso.')
            ];
        }
    }
}