<?php

namespace CodeonWeekends\Eventon\Controllers;

use CodeonWeekends\Eventon\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class SearchController extends BaseController
{
    public function index (Request $request)
    {
        return view('eventon::layouts.default');
    }

    public function search(Request $request)
    {
        $queryString = $request->has('sq') ? $request->sq : '';

        if (!$queryString) {
            $results = Event::limit(30)->get();
        } else {
            $qs = $queryString . '%';
            $results = Event::where('status', true)
                ->where([['begin_time', '>', Carbon::now()]])
                ->where(function ($query) use ($qs) {
                    $query->where([['title', 'like', $qs]])
                        ->orWhere([['location', 'like', $qs]])
                        ->orWhere([['location_city', 'like', $qs]])
                        ->orWhere([['description', 'like', $qs]]);
                })
                ->limit(30)
                ->get();
        }

        return $results;
    }
}