<?php

namespace CodeonWeekends\Eventon\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AccountController extends BaseController
{
    public function index(Request $request)
    {
        return view('eventon::layouts.default');
    }

    public function updatePassword (Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'new_password' => 'required|min:6|confirmed'
        ]);

        if (Hash::check($request->old_password, auth()->user()->password)) {
            if (auth()->user()->update(['password' => Hash::make($request->new_password)])) {
                return response([
                    'message' => __('Palavra passe atualizada com sucesso!')
                ]);

                // TODO: Log user out
            }
        } else {
            return response([
                'message' => 'A Palavra passe atual introduzida é invalida.'
            ], 403);
        }
    }
}