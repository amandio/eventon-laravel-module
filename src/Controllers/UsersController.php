<?php

namespace CodeonWeekends\Eventon\Controllers;

use CodeonWeekends\Eventon\Models\User;
use CodeonWeekends\Eventon\Models\UserInterest;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends BaseController
{
    public function storeInterests(Request $request)
    {
        $interests = [];

        foreach ($request->interests as $id) {
            try {
                $ui = new UserInterest();
                $ui->user_id = $request->user_id;
                $ui->interest_id = $id;
                $ui->save();

                $interests[] = $ui;
            } catch (QueryException $e) {
                return response()->json([
                    'code' => 'B100',
                    'message' => 'Falha ao adicionar interesse.'
                ], 400);
            }
        }

        if (sizeof($interests)) {
            return response()->json([
                'code' => 'B00',
                'message' => '',
                'data' => $interests
            ]);
        }
    }

    public function getInterests(Request $request)
    {
        return $request->user('api')->interests;
    }
}