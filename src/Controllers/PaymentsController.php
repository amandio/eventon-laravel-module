<?php

namespace CodeonWeekends\Eventon\Controllers;

use CodeonWeekends\MPesa\MPesa;
use Illuminate\Support\Str;

class PaymentsController
{
    /**
     * @var $processed
     */
    private $processed;

    /**
     * @var float $tax
     */
    private float $tax;

    /**
     * @var float $total_amount
     */
    private float $total_amount;

    /**
     * @var string $reference
     */
    private string $reference;

    /**
     * @var string $currency
     */
    private string $currency;

    /**
     * VAT percentage
     */
    private float $vat = 17; // Default is 17% for Mozambique

    /**
     * @var object $response
     */
    private object $response;

    /**
     * This functions calculates the amount, currency and the required taxes
     * to be processed by the payment partners.
     *
     * @param float $amount
     * @param string $currency
     * @param float $taxes extra taxes that can be summed with the total amount
     * @param string $reference
     * @return PaymentsController
     */
    public function process(float $amount, string $currency = 'USD', float $taxes = 0, string $reference = '') : PaymentsController
    {
        $vat_percentage = $this->vat < 100 ? 0 . '.' . $this->vat : $this->vat;
        $vat_amount = round($amount * $vat_percentage);
        $this->total_amount = round($amount + $vat_amount + $taxes);
        $this->currency = $currency;

        // Generate random string to use as payment reference
        $this->reference = $reference ?? substr(sha1(date('now')),0,6);

        return $this;
    }

    public function send() : void
    {
        $this->response = ($this->processed)();
    }

    /**
     * @param string $msisdn
     * @return PaymentsController
     */
    public function mpesa(string $msisdn) : PaymentsController
    {
        $this->processed = function () use ($msisdn) {
            $mpesa = new MPesa();

            return $mpesa->c2b($this->reference, $this->total_amount, $msisdn, Str::random(6));
        };

        return $this;
    }

    public function visa($c_holder, $c_number, $c_cvv, $c_month, $c_year)
    {
        return $this;
    }

    /**
     * @return float
     */
    public function getVat(): float
    {
        return $this->vat;
    }

    /**
     * @param float $vat
     */
    public function setVat(float $vat): void
    {
        $this->vat = $vat;
    }

    /**
     * @return mixed
     */
    public function getProcessed()
    {
        return $this->processed;
    }

    /**
     * @param mixed $processed
     */
    public function setProcessed($processed): void
    {
        $this->processed = $processed;
    }

    /**
     * @return float
     */
    public function getTax(): float
    {
        return $this->tax;
    }

    /**
     * @param float $tax
     */
    public function setTax(float $tax): void
    {
        $this->tax = $tax;
    }

    /**
     * @return float
     */
    public function getTotalAmount(): float
    {
        return $this->total_amount;
    }

    /**
     * @param float $total_amount
     */
    public function setTotalAmount(float $total_amount): void
    {
        $this->total_amount = $total_amount;
    }

    /**
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference(string $reference): void
    {
        $this->reference = $reference;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return object
     */
    public function getResponse(): object
    {
        return $this->response;
    }

    /**
     * @param object $response
     */
    public function setResponse(object $response): void
    {
        $this->response = $response;
    }
}