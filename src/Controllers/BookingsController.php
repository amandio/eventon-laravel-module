<?php

namespace CodeonWeekends\Eventon\Controllers;

use App\Models\Transaction;
use CodeonWeekends\Eventon\Events\BookingComplete;
use CodeonWeekends\Eventon\Models\Booking;
use CodeonWeekends\Eventon\Models\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class BookingsController extends Controller
{
    public function getUserBooking(Request $request)
    {
        if ($request->ajax() || $request->wantsJson()) {
            $booking =$request->user('api')
                ->bookings('eventon')
                ->where('number', $request->number)
                ->with('event', 'ticket')
                ->first();

            return response()->json($booking);
        }
    }

    public function getUserBookings(Request $request)
    {
        if ($request->ajax() || $request->wantsJson()) {
            $bookings =$request->user('api')
                ->bookings('eventon')
                ->orderBy('created_at', 'desc');

            // TODO: Paginate results ( $request->p )

            // limit results
            if ($request->has('l')) {
                $bookings->limit($request->l);
            }

            return response()->json($bookings->with('event')->get());
        }
    }

    public function default(Request $request)
    {
        if ($request->has('_b')) {
            if (Auth::check()) {
                $numbers = explode(',', base64_decode($request->get('_b')));
                $bookings = [];

                foreach ($numbers as $booking_number) {
                    $booking = Booking::where([
                        'number' => $booking_number,
                        'contact_email' => Auth::user()->email
                    ])->first();

                    if ($booking) {
                        $bookings[] = $booking;
                    }
                }

                if (sizeof($bookings)) {
                    return view('eventon::bookComplete', compact('bookings'));
                }
            }
        }
        return view('eventon::index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $bookings = Auth::user()->bookings('eventon')->get();;

        return view('eventon::myBookings', compact('bookings'));
    }

    /**
     * Displays the booking details requested by the user
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function checkBookingStatus(Request $request)
    {
        $this->validate($request, [
            'document_number' => 'required',
            'ticket_number' => 'required'
        ]);

        return Booking::where([
            ['contact_document_number', $request->document_number],
            ['number', $request->ticket_number]
        ])->first();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function info(Request $request)
    {
        if ($request->method() == 'POST') {
            Cache::put('tmp_booking_data', $request->items, now()->addMinutes(5));
        }

        $items = $request->method() == 'GET' ? Cache::get('tmp_booking_data') : $request->items;

        if (!$items) {
            return redirect('/');
        }

        $tickets = $this->parseTickets($items);

        return view('eventon::book', compact('tickets'));
    }

    /**
     * @method parseTickets
     * @param $items
     * @return \Illuminate\Support\Collection
     */
    private function parseTickets($items)
    {
        return collect(
            array_map(function ($item) {
                $i = json_decode($item);
                $ticket = Ticket::findOrFail($i->ticket);

                return [
                    'ticket' => [
                        'id' => $ticket->id,
                        'name' => $ticket->name,
                        'price' => $ticket->price,
                        'event' => [
                            'title' => $ticket->event->title
                        ]
                    ],
                    'quantity' => $i->quantity,
                    'total' => $ticket->price * $i->quantity
                ];
            }, $items)
        );
    }

    /**
     * @method getCachedTickets
     * @return \Illuminate\Support\Collection
     */
    public function getCachedTickets()
    {
        $items = Cache::get('tmp_booking_data');

        return $this->parseTickets($items);
    }

    /**
     * Books event(s) and stores the transaction on database
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function book(Request $request)
    {
        $payment_amount = 0;

        if (Cache::has('tmp_booking_data')) {
            // Calculate total
            foreach (Cache::get('tmp_booking_data') as $t) {
                $t = json_decode($t);
                $id = $t->ticket;
                $quantity = $t->quantity;

                $ticket = Ticket::findOrFail($id);
                $payment_amount += $ticket->price * $quantity;
            }

            // If payment amount is 0 bypass payment processing
            if ($payment_amount <= 0 && !$request->has('payment_method')) {
                $response = $this->handleFreeBooking($request);

                if ($response['code'] === 'B00') {
                    return $this->parseSuccessResultAndRedirect($response);
                }
            } else if ($request->has('payment_method') && $request->payment_method === 'm-pesa') {
                $response = $this->handleMpesaPayment($request, $payment_amount);

                if ($response['code'] === 'B00') {
                    Cache::forget('tmp_booking_data');
                    return $this->parseSuccessResultAndRedirect($response);
                }

                return redirect()->back()->withErrors($response);
            }
        } else {
            return redirect('/')->withErrors([ 'expired_time' => __('O tempo maximo para a reserva expirou. Tente novamente')]);
        }
    }

    private function parseSuccessResultAndRedirect($response)
    {
        $bookings = implode(',', array_map(function($booking) {
            return $booking->number;
        }, $response['bookings']));

        return redirect()->route('booking_check', ['_b' => base64_encode($bookings)]);
    }

    private function handleFreeBooking(Request $request)
    {
        $transaction = $this->saveTransaction($request, 0, 'FREETICKET' . rand(1,1000) . strtoupper(Str::random(6)));
        $bookings = $this->saveBooking($request, $transaction);

        return [
            'code' => 'B00',
            'message' => 'Reserva efectuada com sucesso.',
            'bookings' => $bookings
        ];
    }

    /**
     * @param Request $request
     * @param $amount
     * @return array
     */
    private function handleMpesaPayment(Request $request, float $amount) : array
    {
        $payment = new PaymentsController();

        $payment->process($amount, 'MZN', 0, strtoupper(Str::random(6)))
                ->mpesa($request->payment_phone_number)
                ->send();

        $response = $payment->getResponse()
                            ->getBody();

        $reference = $response->output_ConversationID ?? $payment->getReference();
        $transaction = $this->saveTransaction($request, $payment->getTotalAmount(), $reference);

        if ($response->output_ResponseCode === 'INS-0') {
            $bookings = $this->saveBooking($request, $transaction);

            $transaction->update([
                'status' => true
            ]);

            $info = [
                'code' => 'B00',
                'message' => 'Pagamento Efectuado Com Sucesso.',
                'bookings' => $bookings
            ];
        } else {
            $transaction->update([
                'status' => false
            ]);

            $info = [
                'code' => 'B200',
                'message' => __('Falha Ao Processar o Pagamento.'),
                'data' => $response
            ];
        }

        return $info;
    }

    /**
     * @param Request $request
     * @param $transaction
     * @return array
     */
    private function saveBooking(Request $request, $transaction = null)
    {
        $user = $request->user();
        $tickets = $request->tickets;
        $counter = 0;
        $printOptions = array_map(function ($option) {
            return [$option => true];
        }, $request->print_options);

        $bookings = array_map(function($t) use ($counter, $request, $user, $transaction, $printOptions) {
            $counter++;
            $details = explode(',', $t);
            $id = $details[0];
            $quantity = $details[1];
            $ticket = Ticket::find($id);
            $event = $ticket->event;

            return Booking::create([
                'number' => $this->generateTicketNumber($counter),
                'contact_full_name' => $request->contact_full_name,
                'contact_phone' => $request->contact_phone_number,
                'contact_email' => $request->contact_email,
                'contact_document_number' => $request->doc_number,
                'user_id' => $user->id,
                'transaction_id' => $transaction ? $transaction->id : 1,
                'status' => true,
                'confirmed_at' => Carbon::now(),
                'event_id' => $event->id,
                'ticket_id' => $ticket->id,
                'allowed_entrances' => $quantity,
                'print_at_home' => isset($printOptions['print_at_home']),
                'send_to_watsapp' => isset($printOptions['send_to_watsapp']),
                'sent_to_watsapp_at' => null,
                'print_on_site' => isset($printOptions['print_on_site'])
            ]);
        }, $tickets);

        event(new BookingComplete($bookings));

        return $bookings;
    }

    protected function generateTicketNumber($counter = 0)
    {
        $t = strtoupper(substr(sha1(Carbon::now() . $counter), 0, 12));

        if (Booking::find($t)) {
            return $this->generateTicketNumber();
        }
        return $t;
    }

    /**
     * @param Request $request
     * @param $amount
     * @param $reference
     * @return mixed
     */
    private function saveTransaction(Request $request, $amount, $reference)
    {
        $details = [
            'type' => 'payment',
            'host_ip' => $request->ip(),
            'host_os' => '---',
            'host_user_agent' => $request->userAgent(),
            'data' => '', //json_encode($response)
            'amount' => $amount,
            'source' => 'mpesa',
            'description' => '---',
            'reference' => $reference,
            'code' => rand(1000, 9999),
            'status' => false
        ];

        return Transaction::create($details);
    }

    /**
     * @param Ticket $ticket
     * @param int $quantity
     * @return bool
     */
    protected function checkAndDecreaseTicketQuantity(Ticket $ticket, int $quantity) : bool
    {
        if ($ticket->quantity >= $quantity) {
            return $ticket->update([
                'quantity' => $ticket->quantity - $quantity
            ]);
        }

        return false;
    }
}
