<?php
/**
 * @copyright 2020.
 * @author Amandio Khuta Nhamande <amandio16@gmail.com>
 * @license MIT
 *
 */

namespace CodeonWeekends\Eventon\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class AppServiceProvider extends IlluminateServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadRoutesFrom(__DIR__.'/../routes/alt-web.php');
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        $this->mergeConfigFrom(__DIR__.'/../config/eventon.php', 'eventon');
        $this->loadMigrationsFrom(__DIR__.'/../migrations');
        $this->loadViewsFrom(__DIR__.'/../views', 'eventon');

        $this->publishes([
            __DIR__.'/../config/eventon.php' => config_path('eventon.php'),
            __DIR__.'/../views' => resource_path('views/vendor/eventon'),
        ]);

        $this->publishes([
            __DIR__.'/../public' => public_path('vendor/eventon'),
            __DIR__.'/../../images' => public_path('/images'),
        ], 'public');

        Config::set('database.connections.eventon', Config::get('eventon.database.connections.mysql'));
    }

    public function register()
    {}
}