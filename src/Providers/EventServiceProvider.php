<?php

namespace CodeonWeekends\Eventon\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        'CodeonWeekends\Eventon\Events\BookingComplete' => [
            'CodeonWeekends\Eventon\Listeners\SendBookingDetails',
            'CodeonWeekends\Eventon\Listeners\SendBookingCompleteSms'
        ]
    ];

    public function boot()
    {
        parent::boot();
    }
}