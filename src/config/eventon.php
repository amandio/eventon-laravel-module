<?php

return [
    'title' => 'Eventon',
    'description' => 'Reserva online de bilhetes de eventos, auto-carro, avião e rent-a-car.',
    'url' => config('app.env') == 'production' ? 'https://eventos.bilheton.com' : 'http://eventos.bilheton.development',
    'database' => [
        'table_prefix' => 'eventon_',
        'connections' => [
            'mysql' => [
                'driver' => 'mysql',
                'host' => env('DB_HOST', '127.0.0.1'),
                'port' => env('DB_PORT', '3306'),
                'database' => env('DB_DATABASE', 'forge'),
                'username' => env('DB_USERNAME', 'forge'),
                'password' => env('DB_PASSWORD', ''),
                'unix_socket' => env('DB_SOCKET', ''),
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => 'eventon_',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
            ]
        ]
    ],
];