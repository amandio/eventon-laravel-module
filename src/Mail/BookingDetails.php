<?php

namespace CodeonWeekends\Eventon\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingDetails extends Mailable
{
    use Queueable, SerializesModels;

    public $bookings;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($bookings)
    {
        $this->bookings = $bookings;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Dados da Reserva')
            ->view('emails.booking_details', [
                'bookings' => $this->bookings
            ]);
    }
}
