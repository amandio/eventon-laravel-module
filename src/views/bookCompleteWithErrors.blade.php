@extends('eventon::layouts.default')

@section('contents')
    <main-navbar></main-navbar>
    <div style="height: 50px; margin-bottom: 30px"></div>

    <div class="container">
        <div class="message-container error">
            <p>{{ $response['message'] }}</p>
            @if(isset($response['error']) && config('app.env') != 'production')
                <p>{{ $response['error']['message'] }}</p>
                <p><a href="/">Ir a Pagina Inicial</a></p>
            @endif
        </div>
    </div>

    <main-footer></main-footer>
@endsection
