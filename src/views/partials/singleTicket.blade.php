<div class="ticket">
    <header>
        <h1 class="title">{{ $booking->event->title }}</h1>

        {{--<div style="float:right;">--}}
        {{--<div class="ticket-number">{{ $info->ticket_number }}</div>--}}
        {{--<div class="barcode">{!! $info->qr_code !!}</div>--}}
        {{--</div>--}}
    </header>

    <section>
        <div class="info-label" style="width: 219px">
            <span>Nome/Name</span>
            <strong>{{ $booking->contact_full_name }}</strong>
        </div>

        {{--<div class="info-label" style="width: 134px">--}}
        {{--<span>Data/Date</span>--}}
        {{--<strong>{{ $info->trip->departure->format('d M Y') }}</strong>--}}
        {{--</div>--}}

        {{--<div class="info-label">--}}
        {{--<span>Hora/Hour</span>--}}
        {{--<strong>{{ $info->trip->departure->format('H:i') }}</strong>--}}
        {{--</div>--}}
    </section>
</div>
