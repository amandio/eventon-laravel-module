<!Doctype>
<html lang="Pt">
<head>
    @include('eventon::partials.ga')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />

    <title>{{ $title = $title ?? config('eventon.title') }}</title>

    <meta name="theme-color" content="#ffffff" />
    <meta name="keywords" content="eventos da atualidade, compre bilhetes online, bilhete electronico" />
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="{{ $title }}">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="msapplication-navbutton-color" content="#D00202">
    <meta name="msapplication-TileColor" content="#ffffff">

    <link rel="manifest" href="{{ asset('vendor/eventon/manifest.json') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('vendor/eventon/img/touch/apple/apple-touch-icon.png') }}">
    <link rel="mask-icon" href="{{ asset('vendor/eventon/img/touch/apple/safari-pinned-tab.svg') }}" color="#d00202">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('vendor/eventon/favicon-16x16.png') }}" />
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('vendor/eventon/favicon-32x32.png') }}" />
    <link rel="icon" type="image/png" sizes="128x128" href="{{ asset('vendor/eventon/img/touch/android/128x128.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('vendor/eventon/img/touch/android/192x192.png') }}">
    <link rel="shortcut icon" href="{{ asset('vendor/eventon/favicon.ico') }}" type="image/x-icon">

    <meta property="fb:app_id" content="185818735336980" />

    @if (Route::is('single-event'))
        <meta name="description" content="{{ $event->description }}"/>
        <meta property="og:title" content="{{ $event->title }}"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="{{ url('/event/' . $event->alias) }}"/>
        <meta property="og:image" content="{{ url($event->cover_thumbnail) }}"/>
        <meta property="og:description" content="{{ $event->description }}"/>
    @else
        <meta name="description" content="{{ config('eventon.description') }}">
        <meta property="og:title" content="O teu Bilhete Online"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="{{ config('eventon.url') }}"/>
        <meta property="og:image" content="{{ url('/images/hero.jpg') }}"/>
        <meta property="og:description" content="{{ config('eventon.description') }}"/>
    @endif

    <link rel="stylesheet" href="{{ config('app.env') === 'local' ? asset('vendor/eventon/css/main.css') : mix('vendor/eventon/css/main.css') }}">
    <link rel="stylesheet" href="{{ config('app.env') === 'local' ? asset('vendor/eventon/css/components.css') : mix('vendor/eventon/css/components.css') }}">
</head>
<body>
<div id="app"></div>

<script type="text/javascript" src="{{ asset('vendor/eventon/js/manifest.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/eventon/js/vendor.js') }}"></script>
<script type="text/javascript" src="{{ config('app.env') === 'local' ? asset('vendor/eventon/js/app.js') : mix('vendor/eventon/js/app.js') }}"></script>
</body>
</html>
