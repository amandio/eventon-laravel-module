<!DOCTYPE html>
<html lang="pt-PT">
<head>
    @include('eventon::partials.ga')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="theme-color" content="#ffb622">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('vendor/eventon/favicon-16x16.png') }}" />
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('vendor/eventon/favicon-32x32.png') }}" />
    <title>@lang('Administration | Bilheton')</title>

    {{-- Stylesheets --}}
    @yield('custom_header_styles')

    <link rel="stylesheet" href="{{ config('app.env') === 'local' ? asset('vendor/eventon/css/admin/app.css') : mix('vendor/eventon/css/admin/app.css') }}">
</head>
<body>
    <div id="app"></div>

    <script type="text/javascript" src="{{ asset('/vendor/eventon/js/manifest.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendor/eventon/js/vendor.js') }}"></script>
    <script type="text/javascript" src="{{ config('app.env') === 'local' ? asset('/vendor/eventon/js/admin/app.js') : mix('/vendor/eventon/js/admin/app.js') }}"></script>
</body>
</html>
