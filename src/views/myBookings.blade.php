@extends('layouts.admin')

@section('content')

    <div class="content-wrapper account page-trips">
        <h1 class="content-title">Minhas Reservas</h1>

        @if($bookings->count() <= 0)
            <p style="text-align: center;">Nenhuma reserva foi encontrada.</p>
        @else
            <table>
                <thead>
                <tr>
                    <th># Bilhete</th>
                    <th>Evento</th>
                    <th>Nome da Reserva</th>
                    <th>Contacto</th>
                    <th>Numero de Doc</th>
                    <th>Data da Reserva</th>
                    <th>Estado</th>
                    <th>Opções</th>
                </tr>
                </thead>

                <tbody>
                @foreach($bookings as $booking)
                    <tr class="booking">
                        <td><a href="#">{{ $booking->number }}</a></td>
                        <td class="booking-event">
                            <a href="#">{{ $booking->event->title }}</a>
                            <div class="booking-event-details" style="display: none">
                                <div>{{ $booking->event->location }}</div>
                                <div>{{ $booking->event->begin_time->format('d M Y H:i') }}</div>
                                <div>{{ $booking->event->end_time->format('d M Y H:i') }}</div>
                            </div>
                        </td>
                        <td>{{ $booking->contact_full_name }}</td>
                        <td>{{ $booking->contact_phone }}</td>
                        <td>{{ $booking->contact_document_number }}</td>
                        <td>{{ $booking->created_at->format('d M Y') }}</td>
                        <td>{{ $booking->status ? 'confirmado' : 'Pendente' }}</td>
                        <td>
                            <button>i</button>
                            <button>e</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </div>
@endsection
