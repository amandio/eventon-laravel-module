<!Doctype html>
<html lang="pt_Pt">
<head>
    <style>
        * {
            font-family: Helvetica, Arial, sans-serif;
            color: #777777;
            text-align: center;
        }

        h1 {
            color: #333333;
        }

        a {
            color: dodgerblue;
        }
    </style>
</head>
<body>
<h1 class="page-title">Dados da Reserva</h1>

@foreach($bookings as $booking)
    <p>Bilhete {{ $booking->number }}</p>
@endforeach
</body>
</html>