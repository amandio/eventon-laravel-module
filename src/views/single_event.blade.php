@extends('eventon::layouts.default')

@section('contents')
    <router-view event="{{ $event }}"></router-view>
@endsection