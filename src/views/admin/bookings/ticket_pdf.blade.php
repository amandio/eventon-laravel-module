<!Doctype html>
<html lang="pt">
<head>
    <meta name="charset" content="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>{{ $booking->number }}</title>

    <style type="text/css">
        body {
            font-family: 'Roboto', sans-serif;
            color: #353535;
            font-size: 13px;
        }

        .wrapper {
            max-width: 850px;
            margin: auto;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        .company-info td {
            padding-bottom: 30px
        }

        .client-info td {
            height: 100px;
            vertical-align: top;
        }

        td.to-right {
            text-align: right;
        }

        td.to-center {
            text-align: center;
        }

        h2 {
            margin: 0;
        }

        .summary {
            vertical-align: center;
        }

        .summary td {
            padding-bottom: 10px;
        }

        .legend td {
            height: 30px;
        }

        .products td {
        }

        .qrcode {
            width: 110px;
            padding: 8px;
        }

        .price {
            font-weight: bold;
        }

        .iva td {
            padding: 15px 0 10px;
        }

        .total {
            font-size: 19px;
        }

        .total strong, .iva strong {
            color: #777777;
        }

        .generated-date {
            color: #D00202;
        }

        .ticket-detail > * {
            margin-bottom: 3px;
        }

        .ticket-detail strong span {
            opacity: .8;
            font-weight: normal;
        }

        .footer {
            text-align: center;
            font-size: 11px;
            margin-top: 30px;
        }

        .footer p {
            margin: 0;
        }

        .separator td {
            background-color: #cccccc;
            border-top: 1px solid #cccccc;
            height: 1px;
            padding: 0;
            box-sizing: border-box;
        }

    </style>
</head>
<body>
<div class="wrapper">
    <table>
        <tr class="company-info">
            <td colspan="2">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAAAXCAMAAADZXVr9AAACTFBMVEUAAAAAAAAAAAAAAFUAAEAAADMAACsAACQAHDkAGjMAFSsAFCcAEiQAETMAEDAADy0ADSgADTMADDEADC4AFiwAFSsAFCkAFDEAEy8AEi4AESsAECkAEDAADywADisAFC4AEy0AEywAEisGEi8GES4GESwFECsFECsFEC8FDy4FDy0FEysFEy8FEy4FEi4EEi0EEiwEES8EES4EEC0EEC0EECwEECsEDy4EEy0EEiwEEi8EEi4DES0DESwDES4DEC4DECwDEi4DEi4DEi0DEiwDEiwDES4DES4DES0DESwDEC4DEC4DEC0DEC0DEiwDEiwDEi4CES0CESwCES4CES0CES0CECwCECwCEC4CEC4CEi0CEi0CEiwCES4CES0CESwCES4CES4CEC0CEC0CECwEEi4EEi0EESwEESwEES4EES0EES0EES0EESwEES4EEC4EEC0EEi0DES0DES0DESwDESwDES0DES0DES0DECwDEC4DEi0DEi0DESwDES4DES0DES0DESwDES4DES0DEC0DEi4DEi0DESwDES0DES0DES0DESwDEC0DEC0DEi0DEi0DES0DES0DES0DESwDES4DES0DES0DECwDEC0DEi0DEi0CES0CES0CES0CES0CES0CES0EES0EES0EES0EEiwDES0DES0DES0DES0DES0DES0DES0DES0DES0DES0DEi0DES0DES0DES0DES0DES0DES0DES0DES0DES0DES0DES0DES0DES0DES0DES0DES0DES0DES0DES3////scZAsAAAAwnRSTlMAAQIDBAUGBwkKDA0ODxARExQVFhcYGRobHB4fICMkJygpKissLi8wMTIzNTY3ODk6PD0+P0BBQ0RGR0hKTE1OUlNUVVZXWFlaXF5fYGFiY2RnaGprbG1ub3BxcnR1eHl6e3x9f4CBhIWGh4iJiouMjo+Sk5WWmJmam5yfoKGio6anqKmrra6ytba3uLq7vL2/wMLDxMbIycrLzM3O0NLT1NXW19rb3d/h4uPk5ebn6ers7e7v8PLz9PX29/j6+/z9/ju7vNEAAAABYktHRMOKaI5CAAADOklEQVRIx7WW61tUVRSHfzPgwBROaaBS3rooZXcjIawoQ9KgixkJhFlZSiHaDaeCsjCzG+QllSTCTI8apEXDZQYs5v3L+rDPPsw8M8Nziqffp7UvZ717rbXXfo4WP5GiRzRHrW58N7rv6cW5lqtJ0dDcUA/0GTfT3Tfmgu1f42nVvwe8+Od6a+6Y9k4dW5cD9vqcotnOo671QmqO4qv/V9jyKWB0W2n41vemgcH8/wgLlxakT5QsysuAfQJMrpEkNQE8NbM9ULJkXjZYo+Nle73zvCQVNp6Hqz3l5jOnQ6XRMYh1zJf0uOOMcNlxdknXTQF7XOc/AscktTuOU7OwYwQmum7IhK3gY2seTC6VVPxD8nBD/d4/ks2SFODwPb+fb29oH6BvnnRvNHqSb6LRJ6XHAG62Rwb+jkidwP4hU8Wz4cw0nhgLG6MocURS6KeJKklacDpZISlA/297Q5LyPue5tDTuBGLWyZ0A5QbmaXsmbAubjFFPnaSdbDbDmxKnJAWg04yX8mUa7H2g3zq5HqDOhQ1urW2NAyelapweq1pJkcTX5oPe8Wuk/CsDAddBJ8ukAIkSdzxyIQ12CDhqYSGAbQY2GJbUAsSlaoZPW9VL0md/lUjSkukuSRXssA7qqJUCHLHjgVga7Cug17t+AC8Z2AeSdB9AYebVr6JJklpYJ6mBNvtu7qZJCvCd3dc3lgbrBr73egXgmRTYKoCiTFjeUL8kDVwISmpNrfBrs8HeBs7atVKAh33A9Ba3SLexS5L2sWnm5Vw0G6wFuGp7vxKgzA9sBW9KbcllxlX6i5obthbAvsl7gPGQH5hOXAwGLx6TJNWYAvqA5Q8DPWbp2ivAQfmCbeHBcp6VJM2fPOUTplcAXpWkgi8A1vqDRRIffRgvMnaX7XGFi7PCtlLjrjsARxs2vvwLbmB+YPp0NHbANRdcnjCFiHx7LpQNVk2rO1EWS7m5ZyLZYcfbPO22rUal9XnHpWT3hrseemM4XpU1sqLYeHOVOU/ZOY/Vu1DZYSmadFvt10tBL8zid0aBqUPLs9dMm+NwxpiFzT8DJI9vDCorzIcKVlbcPsvGSHnFzP9N8f2Vd+fc+w/B8A2u1e2+CgAAAABJRU5ErkJggg==" alt="bilheton logo">
            </td>

            <td colspan="2" class="to-right">
                <div><strong>Cliente</strong></div>
                <div>{{ $booking->contact_full_name }}</div>
                <div>{{ $booking->contact_phone }}</div>
            </td>
        </tr>

        <tr class="summary">
            <td class="invoice-number" colspan="3">
                <div># Fatura</div>
                <h2>{{ $booking->transaction->id }}</h2>
            </td>
            <td class="generated-date to-right">
                <strong>{{ \Illuminate\Support\Carbon::now()->format('D d M Y') }}</strong>
            </td>
        </tr>
        <tr class="separator"><td colspan="4"></td></tr>
        <tr class="legend">
            <td class="to-center" colspan="2">Descrição</td>
            <td class="to-center" style="width: 30px">Qt</td>
            <td class="to-center">Valor</td>
        </tr>
        <tr class="separator"><td colspan="4"></td></tr>
        <tr class="products">
            <td class="qrcode to-left">
                <div>{!! $qrcode !!}</div>
                <div>{{ $booking->number }}</div>
            </td>
            <td class="ticket-detail">
                <strong>Bilhete de Entrada / <span>Entrance Ticket</span></strong>
                <div>{{ $booking->ticket->name }}</div>
                <div>{{ $booking->event->title }}</div>
                <div>{{ $booking->event->location }}, {{ $booking->event->location_city }}</div>
                <div>{{ $booking->event->begin_time->format('D d M Y')  }} a partir das {{ $booking->event->begin_time->format('H\hi') }}</div>
                <strong>Entradas/<span>Entrances</span></strong>: {{ $booking->allowed_entrances }} (Levantar na bilheteria)
            </td>
            <td class="to-center">{{ $booking->allowed_entrances }}</td>
            <td style="width: 120px" class="to-right price">{{ $booking->ticket->price * $booking->allowed_entrances }} Mt</td>
        </tr>
        <tr class="separator"><td colspan="4"></td></tr>
        <tr class="iva">
            <td class="to-right" colspan="4">
                <span>IVA(17%):</span>
                <span class="price">{{ $booking->transaction->amount - ($booking->transaction->amount / 1.17) }} Mt</span>
            </td>
        </tr>

        <tr class="total">
            <td class="to-right" colspan="4">
                <span>Total:</span>
                <span class="price">{{ $booking->transaction->amount }} Mt</span>
            </td>
        </tr>
    </table>

    <div class="footer">
        <p>Documento processado por computador. @Bilheton, gestão de entradas.</p>
        <p>&copy; 2020 Todos direitos reservados.</p>
    </div>

    <div class="page-break"></div>
</div>

@if(isset($print) && $print)
    <script type="text/javascript">
        // window.print();

        window.setTimeout(() => {
            window.close();
        }, 300);
    </script>
@endif
</body>
</html>