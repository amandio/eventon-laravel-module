@extends('eventon::layouts.default')

@section('contents')
    <router-link errors="{{ $errors }}" tickets="{{ $tickets }}" token="{{ csrf_token() }}"></router-link>
@endsection
