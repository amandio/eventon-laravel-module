@extends('layouts.booking-info')

@section('contents')
    @php
        $booking = $bookings[0];
    @endphp

    <div id="contents-wrapper" class="booking-complete-info">
        <div class="info">
            <div class="wrapper">
                <h1>Reserva Concluida!</h1>
                <p>A sua reserva foi realizada com exito, veja asseguir os dados da sua viagem. Um email e uma mensagem
                    de texto (SMS) foram enviados para
                    <a href="#">{{ $booking ? $booking->contact_email : $bookings[0]['contact_email'] }}</a> e {{ $booking ? $booking->contact_phone : $bookings[0]['contact_phone'] }}
                    respetivamente,
                    contendo mais informações acerca da reserva. Também pode ver as tuas reservas através do <a href="/mybookings">painel de reservas</a>.
                </p>
                <br>
                <p><strong>Obrigado Pela Preferência, Tenha uma boa diversão!</strong></p>
            </div>
        </div>
    </div>

@endsection
