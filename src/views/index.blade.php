@extends('eventon::layouts.default')

@section('contents')
    <transition name="fade" mode="out-in">
        <router-view></router-view>
    </transition>
@endsection