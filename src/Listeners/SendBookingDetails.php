<?php

namespace CodeonWeekends\Eventon\Listeners;

use CodeonWeekends\Eventon\Events\BookingComplete;
use CodeonWeekends\Eventon\Mail\BookingDetails;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendBookingDetails implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingComplete  $event
     * @return void
     */
    public function handle(BookingComplete $event)
    {
        Mail::to($event->bookings[0]->contact_email)->send(new BookingDetails($event->bookings));
    }
}
