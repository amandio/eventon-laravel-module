<?php

namespace CodeonWeekends\Eventon\Listeners;

use CodeonWeekends\Eventon\Events\BookingComplete;
use GuzzleHttp\Client;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;

class SendBookingCompleteSms implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingComplete  $event
     * @return void
     */
    public function handle(BookingComplete $event)
    {
        $booking = $event->bookings[0];
        $details_url = config('app.url') . '/mybookings';
        $message = __($booking['contact_full_name'] ." obrigado por efectuar a reserva em www.bilheton.com. Veja os detalhes da reserva em " . $details_url . ".");
        $contact_phone = $booking['contact_phone'];
        $msisdn = $contact_phone;
        $operator_prefix = substr($contact_phone, 3, 2);
        $operator_code = 0;

        switch ($operator_prefix) {
            case 82:
            case 83:
                $operator_code = 23;
                break;

            case 84:
            case 85:
                $operator_code = 22;
                break;

            case 86:
            case 87:
                $operator_code = 21;
                break;
        }

        $data = [
            'username' => env('USENDIT_USERNAME', 'bilheton'),
            'password' => env('USENDIT_PASSWORD'),
            'partnerEventId' => '',
            'timezone' => '',
            'partnerMsgId' => '',
            'sender' => '',
            'msisdn' => $msisdn,
            'mobileOperator' => $operator_code,
            'priority' => 0,
            'expirationDatetime' => '',
            'messageText' => $message,
            'scheduleDatetime' => '',
            'beginTime' => '',
            'endTime' => '',
            'workingDays' => 'false',
            'isFlash' => 'false'
        ];

        $url = env('USENDIT_URL', 'https://api.usendit.co.mz/v2/remoteusendit.asmx') . '/SendMessage';

        try {
            $client = new Client();
            $response = $client->request('GET', $url, [
                'query' => $data
            ]);

            Log::debug($response->getBody());
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
        }
    }
}