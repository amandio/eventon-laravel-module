<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('eventon')->create('promoters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('alias')->unique();
            $table->string('branding');
            $table->integer('phone');
            $table->integer('phone_alt');
            $table->string('email');
            $table->string('address');
            $table->string('address_city');
            $table->string('address_province');

            $table->string('representative_name');
            $table->integer('representative_phone');
            $table->integer('representative_phone_alt');
            $table->string('representative_email');
            $table->string('representative_address');
            $table->string('representative_address_city');
            $table->string('representative_address_province');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('eventon.database.table_prefix'). 'promoters');
    }
}
