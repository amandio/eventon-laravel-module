<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('eventon')->create('user_interests', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('interest_id')->unsigned()->nullable();
            $table->unique(['user_id', 'interest_id']);

            $table->foreign('interest_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('eventon')->dropIfExists('user_interests');
    }
}
