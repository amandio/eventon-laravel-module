<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('eventon')->create('bookings', function (Blueprint $table) use ($prefix) {
            $table->string('number')->unique()->primary();
            $table->string('contact_full_name');
            $table->string('contact_phone');
            $table->string('contact_email');
            $table->string('contact_document_number');
            $table->integer('user_id')->unsigned();
            $table->integer('event_id')->unsigned();
            $table->integer('ticket_id')->unsigned();
            $table->integer('allowed_entrances');
            $table->integer('transaction_id')->unsigned();
            $table->boolean('status')->default(true);
            $table->timestamp('confirmed_at')->nullable();
            $table->timestamp('cancelled_at')->nullable();
            $table->boolean('print_at_home')->default(false);
            $table->boolean('send_to_whatsapp')->default(false);
            $table->timestamp('sent_to_whatsapp_at')->nullable();
            $table->boolean('print_on_site')->default(false);
            $table->timestamp('printed_at')->nullable();
            $table->string('print_location')->nullable();
            $table->integer('print_count')->default(0);

            // $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('event_id')->references('id')->on('events');
            $table->foreign('ticket_id')->references('id')->on('tickets');
            $table->foreign('transaction_id')->references('id')->on('transactions');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('eventon.database.table_prefix') . 'bookings');
    }
}
