<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBookingLimitToTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('eventon')->table('tickets', function (Blueprint $table) {
            $table->boolean('is_lotated')->default(false)->after('max_reservation_quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(config('eventon.database.table_prefix') . 'tickets', function (Blueprint $table) {
            $table->dropColumn(['is_lotated']);
        });
    }
}
