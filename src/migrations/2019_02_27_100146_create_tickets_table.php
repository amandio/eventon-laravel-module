<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('eventon')->create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('custom_name')->nullable();
            $table->text('custom_description')->nullable();
            $table->integer('quantity');
            $table->integer('min_reservation_quantity')->default(1);
            $table->integer('max_reservation_quantity');
            $table->double('price')->default(0);
            $table->boolean('status')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('eventon.database.table_prefix') . 'tickets');
    }
}
