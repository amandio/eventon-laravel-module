<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationCityToEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('eventon')->table('events', function (Blueprint $table) {
            $table->string('location_city')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $prefix = config('eventon.database.table_prefix');
        $table = $prefix  . 'events';

        Schema::table($table, function (Blueprint $table) use ($prefix) {
            $table->dropColumn( 'location_city');
        });
    }
}
