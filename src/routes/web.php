<?php

Route::group(['domain' => config('eventon.domain'), 'namespace' => 'CodeonWeekends\Eventon\Controllers', 'middleware' => ['web']], function () {
    Route::get('/login', 'EventsController@index');
    Route::get('/register', 'EventsController@index');
    Route::get('/complete_registration', 'EventsController@index');

    Route::get('/cart', 'EventsController@index');

    Route::get('/', 'EventsController@index');
    Route::get('/events', 'EventsController@events');
    Route::get('/events/{category}', 'EventsController@byCategory');
    Route::get('/event/{event}', 'EventsController@view')->name('single-event');

    Route::get('/book', 'BookingsController@info')->middleware('auth');
    Route::post('/book', 'BookingsController@info')->middleware('auth');
    Route::put('/book', 'BookingsController@book')->middleware('auth');
    Route::get('/book/tickets', 'BookingsController@getCachedTickets')->middleware('auth');

    Route::get('/check', 'BookingsController@default')->name('booking_check');
    Route::post('/check', 'BookingsController@checkBookingStatus')->name('booking_info');
    Route::get('/cancel', 'BookingsController@default');

    Route::get('/search', 'SearchController@index');
    Route::post('/search', 'SearchController@search');

    Route::get('/mybookings', 'BookingsController@index');

    // Admin Routes
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth.admin'], function () {
        Route::get('/', 'IndexController@index');

        Route::get('/events', 'IndexController@index');
        Route::get('/events/{event}/view', 'IndexController@index');
        Route::get('/events/{event}/bookings', 'IndexController@index');
        Route::get('/events/{event}/tickets', 'IndexController@index');
        Route::get('/events/{event}/edit', 'IndexController@index');
        Route::get('/events/create', 'IndexController@index');
        Route::get('/events/tickets', 'IndexController@index');
        Route::get('/events/tickets/{id}/view', 'IndexController@index');

        Route::get('/bookings', 'IndexController@index');
        Route::get('/bookings/create', 'IndexController@index');
        Route::get('/bookings/{booking}/view', 'IndexController@index');
        Route::get('/bookings/{booking}/edit', 'IndexController@index');
        Route::get('/bookings/{booking}/print', 'BookingsController@print');
        Route::get('/bookings/{booking}/print_preview', 'BookingsController@printPreview');
        Route::get('/bookings/{booking}/download', 'BookingsController@download');

        Route::get('/reports', 'IndexController@index');
        Route::get('/reports/{target}', 'IndexController@index');

        Route::get('/promoters', 'IndexController@index');
        Route::get('/promoters/{promoter}/view', 'IndexController@index');
        Route::get('/promoters/{promoter}/edit', 'IndexController@index');
        Route::get('/promoters/create', 'IndexController@index');

        Route::get('/categories', 'IndexController@index');
        Route::get('/categories/{promoter}/view', 'IndexController@index');
        Route::get('/categories/{promoter}/edit', 'IndexController@index');
        Route::get('/categories/create', 'IndexController@index');
        Route::get('/categories/import', 'IndexController@index');
    });

    // Promoter Routes
    Route::group(['prefix' => 'promoter', 'namespace' => 'Promoter', 'middleware' => 'auth.promoter'], function () {
        Route::get('dashboard', 'IndexController@index');
        Route::get('events', 'IndexController@index');
    });

    Route::get('/account', 'AccountController@index')->middleware('auth');
    Route::get('/account/settings', 'AccountController@index')->middleware('auth');
    Route::get('/wallet', 'AccountController@index')->middleware('auth');
    Route::get('/mybookings', 'AccountController@index')->middleware('auth');
    Route::get('/mybookings/{number}', 'AccountController@index')->middleware('auth');
    Route::get('/account/password', 'AccountController@index')->middleware('auth');
    Route::put('/account/password', 'AccountController@updatePassword')->middleware('auth');
    Route::get('/account/favorites', 'AccountController@index')->middleware('auth');
    Route::get('/account/interests', 'AccountController@index')->middleware('auth');

});

