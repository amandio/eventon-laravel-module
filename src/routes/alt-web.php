<?php

$domain = config('app.env') == 'production' ? 'eventos.bilheton.com' : 'eventos.bilheton.development';

Route::group(['prefix' => 'eventon', 'namespace' => 'CodeonWeekends\Eventon\Controllers', 'middleware' => ['web']], function () {
    Route::get('/login', 'EventsController@index');
    Route::get('/register', 'EventsController@index');
    Route::get('/complete_registration', 'EventsController@index');

    Route::get('/cart', 'EventsController@index');

    Route::get('/', 'EventsController@index');
    Route::get('/events', 'EventsController@events');
    Route::get('/events/{category}', 'EventsController@byCategory');
    Route::get('/event/{event}', 'EventsController@view')->name('single-event');

    Route::get('/book', 'BookingsController@info')->middleware('auth');
    Route::post('/book', 'BookingsController@info')->middleware('auth');
    Route::put('/book', 'BookingsController@book')->middleware('auth');
    Route::get('/book/tickets', 'BookingsController@getCachedTickets')->middleware('auth');
    Route::post('/info', 'BookingsController@bookingDetails')->name('booking_info');
    Route::get('/info', 'BookingsController@bookingComplete')->name('booking_complete_info');
    Route::get('/check', 'BaseController@index');
    Route::get('/cancel', 'BaseController@index');

    Route::get('/search', 'SearchController@index');

    Route::get('/mybookings', 'BookingsController@index');

    // Admin Routes
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth.admin'], function () {
        Route::get('/', 'IndexController@index');

        Route::get('/events', 'IndexController@index');
        Route::get('/events/{event}/view', 'IndexController@index');
        Route::get('/events/{event}/tickets', 'IndexController@index');
        Route::get('/events/{event}/edit', 'IndexController@index');
        Route::get('/events/create', 'IndexController@index');

        Route::get('/e/bookings', 'IndexController@index');
        Route::get('/e/bookings/create', 'IndexController@index');
        Route::get('/e/bookings/{booking}/view', 'IndexController@index');
        Route::get('/e/bookings/{booking}/edit', 'IndexController@index');
        Route::get('/e/bookings/{booking}/print', 'BookingsController@print');

        Route::get('/reports', 'IndexController@index');
        Route::get('/reports/{target}', 'IndexController@index');

        Route::get('/promoters', 'IndexController@index');
        Route::get('/promoters/{promoter}/view', 'IndexController@index');
        Route::get('/promoters/{promoter}/edit', 'IndexController@index');
        Route::get('/promoters/create', 'IndexController@index');
    });
});

