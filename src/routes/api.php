<?php

Route::group(['prefix' => 'api', 'namespace' => 'CodeonWeekends\Eventon\Controllers', 'middleware' => ['api']], function () {
    Route::get('/events', 'EventsController@events');
    Route::get('/events/{category}', 'EventsController@byCategory');
    Route::get('/event/{event}', 'EventsController@view');
    Route::get('/event/book', 'EventsController@bookForm');
    Route::post('/event/book', 'EventsController@book');

    Route::get('/categories', 'CategoriesController@index');

    Route::get('/user_bookings', 'BookingsController@getUserBookings');
    Route::get('/user_bookings/{number}', 'BookingsController@getUserBooking');
    Route::get('/qr_code', 'UtilController@generateQrCode');

    Route::post('/user/interests', 'UsersController@storeInterests');
    Route::get('/user/interests', 'UsersController@getInterests');

    Route::group(['middleware' => ['auth:api', 'auth.admin'], 'prefix' => 'admin', 'namespace' => 'Admin'], function () {
        Route::get('/events', 'EventsController@index');
        Route::get('/events/{id}/view', 'EventsController@view');
        Route::post('/events', 'EventsController@store');
        Route::patch('/events', 'EventsController@update');
        Route::delete('/events', 'EventsController@destroy');
        Route::get('/events/{id}/tickets', 'EventsController@tickets');
        Route::put('/events/{id}/tickets', 'TicketsController@update');
        Route::get('/events/{id}/bookings', 'EventsController@bookings');
        Route::post('/events/publish', 'EventsController@publish');

        Route::post('/tickets', 'TicketsController@store');

        Route::get('/promoters', 'PromotersController@index');
        Route::get('/promoters/{promoter}/view', 'PromotersController@view');
        Route::delete('/promoters', 'PromotersController@destroy');
        Route::post('/promoters', 'PromotersController@store');
        Route::patch('/promoters', 'PromotersController@update');

        Route::get('/e/bookings', 'BookingsController@index');
        Route::get('/e/bookings/{number}/view', 'BookingsController@view');
        Route::post('/e/bookings', 'BookingsController@store');
        Route::patch('/e/bookings', 'BookingsController@update');
        Route::delete('/e/bookings', 'BookingsController@destroy');

        Route::get('/categories', 'CategoriesController@index');
        Route::get('/categories/{promoter}/view', 'CategoriesController@view');
        Route::delete('/categories', 'CategoriesController@destroy');
        Route::post('/categories', 'CategoriesController@store');
        Route::patch('/categories', 'CategoriesController@update');

        Route::get('/tickets', 'TicketsController@all');
        Route::get('/tickets/{id}/view', 'TicketsController@view');
    });

    // Promoters Routes
    Route::group(['middleware' => ['auth:api', 'auth.promoter'], 'prefix' => 'promoter', 'namespace' => 'Promoter'], function () {
        Route::get('{id}/booked_events', 'IndexController@getBookedEvents');
        Route::get('{id}/events', 'IndexController@getEvents');
    });
});
