export default {
  cart: (state) => {
    return state.cart
  },

  user: (state) => {
    return state.auth.user
  }
}