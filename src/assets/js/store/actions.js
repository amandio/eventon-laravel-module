const ls = localStorage;
let cart = ls.getItem('cart');

export default {
  updateCurrentCountry(store) {
    ls.setItem('currentCity', store.state.app.country);
  },

  updateCurrentCity(store) {
    ls.setItem('currentCity', store.state.app.city);
  },

  updateCart (store) {
    ls.setItem('cart', JSON.stringify(store.getters.cart))
  },

  emptyCart (store) {
    ls.setItem('cart', '[]');
    store.commit('emptyCart')
  },

  addCartItem (store, item) {
    if (!cart) {
      ls.setItem('cart', '[]');
      cart = ls.getItem('cart');
    }

    store.commit('addCartItem', item);
    ls.setItem('cart', JSON.stringify(store.getters.cart));
  },

  removeCartItem (store, item) {
    let confirm = window.confirm('Tem a certeza que pretende eliminar o item do carrinho?');

    if (confirm) {
      if (cart) {
        store.commit('removeCartItem', item);
        ls.setItem('cart', JSON.stringify(store.getters.cart));
        window.location.reload()
      }
    }
  }
}