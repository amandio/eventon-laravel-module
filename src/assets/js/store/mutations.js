export default {
  emptyCart (state) {
    state.cart = []
  },

  addCartItem (state, item) {
    state.cart.push(item)
  },

  updateAppLanguage (state, language) {
    state.app.language = language
  },

  updateClientCity (state, city) {
    state.app.city = city
  },

  updateClientCountry (state, country) {
    state.app.country = country
  },

  updateCart() {
  },

  removeCartItem (state, item) {
    _.remove(state.cart, item)
  },

  updateUser (state, user) {
    state.auth.user = user
  }
}
