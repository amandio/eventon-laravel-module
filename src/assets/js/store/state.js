import bookingsListColumns from '../components/admin/bookings/tableColumns';
const ls = localStorage;

export default {
  cart: localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [],
  auth: {
    user: null
  },
  app: {
    timezone: 'Africa/Maputo',
    currency: 'Mt',
    language: 'en',
    country: null,
    city: localStorage.getItem('currentCity') ? localStorage.getItem('currentCity') : null,
  },
  redirectUrl: '',
  dataTables: {
    bookingsListTable: {
      showColumns: ls.getItem('bookingsListTableShowColumns') ? JSON.parse(ls.getItem('bookingsListTableShowColumns')) : bookingsListColumns
    }
  }
}