import Index from '../components/Home'
import Login from '../components/Login'
import Register from '../components/Register'
import Cart from '../components/Cart'
import Events from '../components/Events'
import EventView from '../components/Events/SingleEvent'
import Book from '../components/Events/Book'
import RegistrationInterests from "../components/Register/RegistrationInterests";
import BookingCheck from "../components/booking/BookingCheck";
import Search from "../components/Search";
import NotFound from "../components/errors/NotFound";

export default [
  {
    path: '/',
    alias: '/eventon',
    name: 'Home',
    component: Index
  },
  {
    path: '/login',
    alias: '/eventon/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    alias: '/eventon/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/complete_registration',
    alias: '/eventon/complete_registration',
    name: 'RegistrationInterests',
    component: RegistrationInterests
  },
  {
    path: '/cart',
    alias: '/eventon/cart',
    name: 'Cart',
    component: Cart
  },
  {
    path: '/events',
    alias: '/eventon/events',
    name: 'Events',
    component: Events,
    props: true
  },
  {
    path: '/events/:category',
    alias: '/eventon/events/:category',
    name: 'EventsByCategory',
    component: Events,
    props: true
  },
  {
    path: '/event/:alias',
    alias: '/eventon/event/:alias',
    name: 'SingleEvent',
    component: EventView,
    props: true
  },
  {
    path: '/book',
    alias: '/eventon/book',
    name: 'Book',
    component: Book
  },
  {
    path: '/check',
    alias: '/eventon/check',
    name: 'CheckBooking',
    component: BookingCheck
  },
  {
    path: '/cancel',
    alias: '/eventon/cancel',
    name: 'CancelBooking'
  },
  {
    path: '/search',
    name: 'Search',
    component: Search
  },
  {
    path: '*',
    component: NotFound
  }
]
