import Vue from 'vue';
import VueRouter from 'vue-router';
import appRoutes from './routes';
import accountRoutes from '../components/account/routes';

Vue.use(VueRouter);

let routes = appRoutes.concat(accountRoutes);

export default new VueRouter({
  mode: 'history',
  routes
})