import EventsIndex from './events/AdminEventsIndex'
import EventsList from './events/AdminEventsList'
import EventsCreate from './events/AdminEventsCreate'
import EventsView from './events/AdminEventsView'
import EventsEdit from './events/AdminEventsEdit'
import EventsTickets from './events/AdminEventsTickets'

import BookingsIndex from './bookings/AdminBookingsIndex'
import BookingsList from './bookings/AdminBookingsList'
import BookingsCreate from './bookings/AdminBookingsCreate'
import BookingsView from './bookings/AdminBookingsView'
import BookingsEdit from './bookings/AdminBookingsEdit'

import CategoriesIndex from './categories/AdminCategoriesIndex'
import CategoriesList from './categories/AdminCategoriesList'
import CategoriesCreate from './categories/AdminCategoriesCreate'
import CategoriesEdit from './categories/AdminCategoriesEdit'
import CategoriesView from './categories/AdminCategoriesView'
import CategoriesImport from './categories/AdminCategoriesImport';
import AdminDashboardIndex from "./dashboard/AdminDashboardIndex";

import PromotersIndex from './promoters/AdminPromotersIndex';
import PromotersList from './promoters/AdminPromotersList';
import PromotersCreate from './promoters/AdminPromotersCreate';
import PromotersEdit from './promoters/AdminPromotersEdit';
import PromotersView from './promoters/AdminPromotersView';

let routes = [
  {
    path: '/admin',
    name: 'AdminDashboardIndex',
    component: AdminDashboardIndex
  },
  {
    path: '/admin/bookings',
    component: BookingsIndex,
    children: [
      {
        path: '',
        name: 'AdminBookingsList',
        component: BookingsList
      },
      {
        path: 'create',
        name: 'AdminBookingsCreate',
        component: BookingsCreate
      },
      {
        path: ':number/view',
        name: 'AdminBookingsView',
        props: true,
        component: BookingsView
      },
      {
        path: ':number/edit',
        props: true,
        name: 'AdminBookingsEdit',
        component: BookingsEdit
      }
    ]
  },
  {
    path: '/admin/events',
    component: EventsIndex,
    children: [
      {
        path: '',
        name: 'AdminEventsIndex',
        component: EventsList
      },
      {
        path: 'create',
        name: 'AdminEventsCreate',
        component: EventsCreate
      },
      {
        path: ':id/view',
        name: 'AdminEventsView',
        props: true,
        component: EventsView
      },
      {
        path: ':id/edit',
        name: 'AdminEventsEdit',
        props: true,
        component: EventsEdit
      },
      {
        path: ':id/tickets',
        name: 'AdminEventsTickets',
        props: true,
        component: EventsTickets
      },
      {
        path: ':id/bookings',
        name: 'AdminEventsBookings',
        props: true,
        component: AdminEventsBookings
      },
      {
        path: 'tickets',
        name: 'AdminTickets',
        component: AdminTicketsList
      },
      {
        path: 'tickets/:id/view',
        name: 'AdminTicketsView',
        component: AdminTicketsView,
        props: true
      }
    ]
  },
  {
    path: '/admin/categories',
    component: CategoriesIndex,
    children: [
      {
        path: '',
        name: 'AdminCategoriesList',
        component: CategoriesList
      },
      {
        path: 'create',
        name: 'AdminCategoriesCreate',
        component: CategoriesCreate,
      },
      {
        path: ':id/edit',
        name: 'AdminCategoriesEdit',
        component: CategoriesEdit,
        props: true
      },
      {
        path: ':id/view',
        name: 'AdminCategoriesView',
        component: CategoriesView,
        props: true
      },
      {
        path: 'import',
        name: 'AdminCategoriesImport',
        component: CategoriesImport
      }
    ]
  },
  {
    path: '/admin/promoters',
    component: PromotersIndex,
    children: [
      {
        path: '',
        name: 'AdminPromotersList',
        component: PromotersList
      },
      {
        path: 'create',
        name: 'AdminPromotersCreate',
        component: PromotersCreate,
      },
      {
        path: ':id/edit',
        name: 'AdminPromotersEdit',
        component: PromotersEdit,
        props: true
      },
      {
        path: ':id/view',
        name: 'AdminPromotersView',
        component: PromotersView,
        props: true
      }
    ]
  },
  {
    path: '/admin/reports',
    component: AdminReportsIndex,
    children: [
      {
        path: '',
        name: 'AdminReports',
        component: AdminReportsList
      },
      {
        path: ':report',
        name: 'AdminReportsView',
        component: AdminReportsGenerator,
        props: true
      }
    ]
  },
  {
    path: '*',
    component: NotFound
  }
];

import Vue from 'vue';
import VueRouter from 'vue-router';
import AdminCategoriesImport from "./categories/AdminCategoriesImport";
import AdminTicketsList from "./tickets/AdminTicketsList";
import AdminTicketsView from "./tickets/AdminTicketsView";
import AdminEventsBookings from "./events/AdminEventsBookings";
import NotFound from "../errors/NotFound";
import AdminReportsIndex from "./reports/AdminReportsIndex";
import AdminReportsList from "./reports/AdminReportsList";
import AdminReportsGenerator from "./reports/AdminReportsGenerator";

Vue.use(VueRouter);

export default new VueRouter({
  mode: 'history',
  routes
})
