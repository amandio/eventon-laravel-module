export default {
    row_select: { title: 'Check', visible: false },
    event: { title: 'Evento', visible: true },
    number: { title: 'Bilhete Número', visible: true },
    ticket_name: { title: 'Bilhete Tipo', visible: true },
    allowed_entrances: { title: 'Entradas', visible: true },
    contact_full_name: { title: 'Nome', visible: true },
    contact_email: { title: 'E-mail', visible: true },
    contact_phone: { title: 'Contacto', visible: true },
    contact_document_number: { title: '# Documento', visible: true },
    created_at: { title: 'Data de Reserva', visible: true },
    status: { title: 'Estado', visible: true },
    owner_email: { title: 'Usuário', visible: false },
    transaction_status: { title: 'Transação', visible: false },
    transaction_amount: { title: 'Valor', visible: false }
}