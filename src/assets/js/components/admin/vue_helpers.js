import {publishEvent} from "../../helpers";

export function togglePublishEvent(vue, event) {
    vue.loading = true;
    let status = !event.status;

    return publishEvent(event.id, status)
        .catch(err => {
            vue.$root.$emit('errorMessage', err.response.message);
            return err.response;
        })
        .then(res => {
            if (res.status === 200) {
                vue.$root.$emit('successMessage', res.data.message);
            }
        })
        .then(() => vue.loading = false);
}
