import PromoterDashboard from "./PromoterDashboard";
import PromoterEvents from "./PromoterEvents";

export default [
  {
    path: '/promoter/dashboard',
    name: 'PromoterDashboard',
    component: PromoterDashboard
  },
  {
    path: '/promoter/events',
    name: 'PromoterEvents',
    component: PromoterEvents
  }
];
