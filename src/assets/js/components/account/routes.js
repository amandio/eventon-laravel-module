import AccountIndex from './AccountIndex';
import AccountInfo from './AccountInfo';
import AccountSettings from "./AccountSettings";
import MyBookings from "../MyBookings/MyBookings";
import WalletIndex from "../Wallet/WalletIndex";
import MyBookingsDetails from "../MyBookings/MyBookingsDetails";
import AccountPassword from "./AccountPassword";
import AccountFavorites from "./AccountFavorites";
import AccountInterests from "./AccountInterests";

export default [
  {
    path: '/account',
    component: AccountIndex,
    children: [
      {
        path: '/',
        name: 'AccountIndex',
        component: AccountInfo
      },
      {
        path: '/wallet',
        name: 'MyWallet',
        component: WalletIndex
      },
      {
        path: '/mybookings',
        name: 'MyBookings',
        component: MyBookings
      },
      {
        path: '/mybookings/:number',
        name: 'MyBookingsDetails',
        component: MyBookingsDetails,
        props: true
      },
      {
        path: 'settings',
        name: 'AccountSettings',
        component: AccountSettings
      },
      {
        path: 'password',
        name: 'AccountPassword',
        component: AccountPassword
      },
      {
        path: 'favorites',
        name: 'FavoriteEvents',
        component: AccountFavorites
      },
      {
        path: 'interests',
        name: 'MyInterests',
        component: AccountInterests
      }
    ]
  }
]