require('./bootstrap');

import NProgress from 'vue-nprogress'
import router from './router'
import store from './store'
import auth from './auth'
import Meta from 'vue-meta'
import App from './components/App';
import {fetchAuthUser} from "./helpers";

Vue.use(NProgress);
Vue.use(Meta);

const nprogress = new NProgress({
  trickleSpeed: 200,
  showSpinner: false,
  easing: 'ease',
  speed: 200,
  minimum: 0.01
});

router.beforeEach((to, from, next) => {
  nprogress.start();
  /**
   * Check if user is authenticated and fetch the user info from database
   * This doesn't work properly when added to the <b>created</b> hook in the <App> component
   */
  fetchAuthUser()
    .catch(err => {
      return err.response;
    })
    .then(res => {
      if (res.status === 200) {
        store.commit('updateUser', res.data);
      } else {
        store.commit('updateUser', null);
      }
    })
    .then(() => next());
});

router.afterEach(() => {
  nprogress.done();
  window.scrollTo(0, 0);
});

const app = new Vue({
  el: '#app',
  router,
  store,
  auth,
  nprogress,
  component: App,
  template: '<App/>'
});