import store from './store';

export function storeBooking(booking) {
  return axios({
    url: '/api/admin/e/bookings',
    method: 'POST',
    data: booking
  });
}

export function fetchTicketById(id) {
  return axios.get(`/api/admin/tickets/${id}/view`);
}

export function fetchTickets() {
  return axios.get(`/api/admin/tickets`);
}

export function fetchBookingsByEventId(id, params) {
  return axios({
    url: `/api/admin/events/${id}/bookings`,
    params
  });
}

export function publishEvent(id, status) {
  return axios({
    url: '/api/admin/events/publish',
    method: 'POST',
    data: { id, status }
  });
}

export function generateQrCode(content, width, height) {
  return axios({
    url: '/api/qr_code',
    method: 'GET',
    params: {
      content,
      width,
      height
    }
  });
}

export function fetchAuthUserBooking(bookingNumber) {
  return axios({
    url: `/api/user_bookings/${bookingNumber}`,
    method: 'GET'
  });
}


export function fetchAuthUserBookings(params) {
  return axios({
    url: '/api/user_bookings',
    method: 'GET',
    params
  });
}

export function storeCategory(data) {
  return axios({
    url: '/api/admin/categories',
    method: 'POST',
    data
  });
}

export function fetchEventById (id) {
  return axios.get('/api/admin/events/' + id + '/view')
}

export function fetchTicketsByEventId (id) {
  return axios.get('/api/admin/events/' + id + '/tickets')
}

export function updateTicket(eventId, ticket) {
  return axios({
    url: `/api/admin/events/${eventId}/tickets`,
    method: 'PUT',
    data: {
      ...ticket
    }
  });
}

export function fetchPromoters(query) {
  let qs = query !== undefined ? '?qs=' + query : ''

  return axios({
    url: '/api/admin/promoters',
    params: {
      qs
    }
  });
}

export function fetchPromoterById(id) {
  return axios(`/api/admin/promoters/${id}/view`);
}

export function fetchBookings(qs, params) {
  return axios({
    url: '/api/admin/e/bookings',
    params: {
      qs,
      ...params
    }
  });
}

export function fetchBookingByNumber(number, params) {
  return axios({
    url: `/api/admin/e/bookings/${number}/view`,
    params
  });
}

export function storeEvent(data) {
  return axios({
    url: '/api/admin/events',
    method: 'POST',
    data: {
      event: data
    }
  });
}

export function updateEvent(data) {
  return axios({
    url: '/api/admin/events',
    method: 'PATCH',
    data: {
      event: data
    }
  });
}

export function storeEventTicket(eventId, ticket) {
  return axios({
    url: '/api/admin/tickets',
    method: 'POST',
    data: {
      event_id: eventId,
      ticket
    }
  });
}

export function deleteEvent(id) {
  if (window.confirm('Tem a certeza que deseja eliminar o Evento?')) {
    return axios({
      url: '/api/admin/events',
      method: 'DELETE',
      data: {
        id
      }
    });
  }

  window.location.reload();
}

export function deletePromoter(id) {
  if (window.confirm('Tem a certeza que deseja eliminar o Promotor?')) {
    return axios({
      url: '/api/admin/promoters',
      method: 'DELETE',
      data: {
        id
      }
    });
  }

  window.location.reload();
}

export function deleteBooking(number) {
  if (window.confirm('Tem a certeza que deseja eliminar a Reserva?')) {
    return axios({
      url: '/api/admin/e/bookings',
      method: 'DELETE',
      data: {
        number
      }
    });
  }

  window.location.reload();
}

export function generateSlug(text) {
  return axios({
    url: '/api/util/generate_slug',
    params: {
      text
    }
  });
}

export function storePromoter(data) {
  return axios({
    url: '/api/admin/promoters',
    method: 'POST',
    data
  });
}

export function updatePromoter(data) {
  return axios({
    url: '/api/admin/promoters',
    method: 'PATCH',
    data
  });
}

/**
 * Handle error responses from ajax call
 *
 * @param vue is current Vue instance
 * @return {Function}
 */
export function errorResponse(vue) {
  return err => {
    vue.$root.$emit('errorMessage', err.response.data)
  }
}

export function fetchPromoterEvents(promoterId) {
  return axios({
    url: `/api/promoter/${promoterId}/events`
  });
}

export function fetchPromoterBookedEvents(promoterId) {
  return axios({
    url: `/api/promoter/${promoterId}/booked_events`
  });
}

export function fetchAdminEvents (params) {
  return axios({
    url: '/api/admin/events',
    params: {
      fetch_all: 1,
      ...params
    }
  })
}

export function fetchEvents (page, domain, city) {
  let p = page !== undefined ? page : 1;
  let d = domain !== undefined ? domain : 0;

  return axios({
    url: '/api/events',
    params: {
      p,
      d,
      city
    }
  })
}

export function fetchEventsByCategory (category, page, city) {
  let p = page !== undefined ? page : 1
  return axios({
    url: '/api/events/' + category,
    params: {
      p,
      city
    }
  })
}

/**
 * Fetches a single event from database
 *
 * @param alias
 * @return {*}
 */
export function fetchEventByAlias (alias) {
  return axios.get('/api/event/' + alias)
      .then(res => {
        return res.data
      })
      .catch(err => {
        return err.response
      })
}

export function lazyLoadImages() {
  let imgs = document.querySelectorAll('img');

  _.each(imgs, img => {
    let src = img.dataset.src
    if (src !== undefined) {
      let i = new Image()
      i.src = src
      i.onload = () => {
        img.src = src
        img.removeAttribute('data-src')
      }
    }
  })
}

export function toggleNavbarShadowVisibility(navbar, height) {
  if (navbar) {
    if (window.scrollY >= height && !navbar.classList.contains('shadowed')) {
      navbar.classList.add('shadowed')
    } else if (window.scrollY < height) {
      navbar.classList.remove('shadowed')
    }
  }
}

export function getClientLanguage () {
  if ('language' in navigator) {
    store.commit('updateAppLanguage', navigator.language);
  }
}


export function fixNavbarOnScroll(navbar, height) {
  document.addEventListener('scroll',function () {
    if (navbar) {
      if (window.scrollY >= height && !navbar.classList.contains('fix')) {
        navbar.classList.add('fix')
      } else if (window.scrollY < height) {
        navbar.classList.remove('fix')
      }
    }
  })
}

export function getClientCity () {
  function setDefaultLocation() {
    if (!store.state.app.city) {
      store.commit('updateClientCity', 'all');
    }

    store.commit('updateClientCountry', 'Mozambique');
  }

  function fetchCityCountryInfo(lat, long) {
    return axios({
      method: 'GET',
      url: 'https://open.mapquestapi.com/geocoding/v1/reverse',
      params: {
        key: 'wnySqPpUoCln92bifY7yuwMzb5HMHVpK',
        location: `${lat},${long}`,
        includeRoadMetadata: true,
        includeNearestIntersection: true
      }
    })
        .then(res => {
          let location = res.data.results[0].locations[0];

          return {
            city: location.adminArea5,
            country: location.adminArea1
          }
        });
  }

  function success(response) {
    // fetchCityCountryInfo(response.coords.latitude, response.coords.longitude)
    //   .then(data => {
    //     if (!store.state.app.city) {
    //       store.commit('updateClientCity', data.city);
    //       store.dispatch('updateCurrentCity');
    //     }
    //
    //     if (!store.state.app.country) {
    //       store.commit('updateClientCountry', data.country);
    //       store.dispatch('updateCurrentCountry');
    //     }
    //   });
  }

  function error() {
    // Todo: Report Error
    setDefaultLocation();
  }

  let options = {
    timeout: 27000
  };

  if ('geolocation' in navigator) {
    navigator.geolocation.getCurrentPosition(error, error, options)
  }
}

export function authenticateUser (email, password) {
  return axios({
    url: '/login',
    method: 'post',
    type: 'json',
    data: {
      email,
      password
    }
  })
}

export function register (data) {
  return axios({
    url: '/register',
    method: 'post',
    type: 'json',
    data
  })
}

export function fetchAuthUser () {
  return axios({
    url: '/api/user',
    method: 'get'
  })
}

export function updateUserPassword(oldPassword, newPassword, passwordConfirmation) {
  return axios({
    url: '/account/password',
    method: 'PUT',
    data: {
      old_password: oldPassword,
      new_password: newPassword,
      new_password_confirmation: passwordConfirmation
    }
  });
}

export function logout () {
  return axios({
    url: '/logout',
    type: 'json'
  })
}

export function fetchCategories(params) {
  return axios({
    url: '/api/categories',
    params
  });
}

// export function fetchCategories(qs) {
//   return axios({
//     url: '/api/admin/categories',
//     params: qs
//   });
// }

export function saveUserInterests(data) {
  return axios({
    url: '/api/user/interests',
    method: 'POST',
    data
  });
}

export function validateMozambiqueMobileNumber(number, includeCountryCode) {
  let regex = /^(?<country_code>258)?(?<prefix>82|83|84|85|86|87)[0-9]{7}$/

  return regex.test(number)
}

export function validateMozambiqueVodacomMobileNumber(number) {
  let regex = /^(?<country_code>258)?(?<prefix>84|85)[0-9]{7}$/

  return regex.test(number)
}

export function regexToObject($regex, $str) {
  let i, result

  while ((i = regex.exec(str)) !== null) {
    if (i.index === regex.lastIndex) {
      regex.lastIndex++;
    }

    i.forEach((match, groupIndex) => {
      result[groupIndex] = match
    })
  }

  return result
}


export function fileUpload(files, location, disk) {
  let fd = new FormData();
  for (let file of files) {
    fd.append('files[]', file);
  }

  if (typeof disk != 'undefined') {
    fd.append('disk', disk);
  }

  return axios({
    url: '/api/util/file/upload',
    method: 'post',
    data: fd,
    type: 'multipart/form-data'
  });
}

export function initDatePicker(selector, onSelect) {
  selector = selector ? selector : '.datepicker';

  let options = {
    dayNamesShort: ['Seg', 'Ter', 'Qua', 'Qui', 'Ses', 'Sab', 'Dom'],
    dayNamesMin: ['Se', 'Te', 'Qa', 'Qi', 'Se', 'Sa', 'Do'],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    hideIfNoPrevNext: true,
    minDate: 0,
    numberOfMonths: 2,
    nextText: '<i class="icon icon-next"></i>',
    prevText: '<i class="icon icon-back"></i>',
    dateFormat: 'dd/mm/yy',
    onSelect
  };

  return $(selector).datepicker(options);
}

export function initTimePicker(selector, onSelect) {
  selector = selector ? selector : '.timepicker';

  $(selector).datetimepicker({
    showButtonPanel: false,
    timeText: 'Horas',
    hourText: 'Hora',
    minuteText: 'Minuto',
    timeOnly: true,
    timeOnlyTitle: 'Escolha a Hora',
    onSelect
  });
}
