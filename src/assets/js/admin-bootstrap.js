window._ = require('lodash');
window.moment = require('moment');
moment.locale('pt');
window.accounting = require('accounting');
accounting.settings.currency = {
    symbol: 'Mt',
    format: '%v %s',
    precision: 0
};

window.dt = require('datatables.net');

try {
    window.$ = window.jQuery = require('jquery');
    require('jquery-ui/ui/widgets/datepicker');
    require('jquery-ui/ui/widgets/slider');
    require('jquery-ui/ui/widgets/autocomplete');
    require('jquery-ui-timepicker-addon/dist/jquery-ui-timepicker-addon');
    // window.Popper = require('popper.js').default;
} catch (e) {}

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.timeout = 30000;

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

window.Vue = require('vue');

/**
 * Handle automatic compile of .vue files
 */
const files = require.context('./components/admin', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));