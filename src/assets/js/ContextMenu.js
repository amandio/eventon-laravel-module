class ContextMenu {
  constructor(options) {
    this.menuClasses = options ? options.menuClasses : 'context-menu-wrapper';
    this.menuId = options ? options.menuId : 'context-menu';
    this.menu = options ? options.menu : {};
    this.position = {
      x: 0,
      y: 0
    };

    // Context menus
    this.view = {
      label: 'Ver',
    };

    this.edit = {
      label: 'Editar',
    };

    this.confirm = {
      label: 'Confirmar',
    };

    this.cancel = {
      label: 'Cancelar',
    };

    this.delete = {
      label: 'Apagar',
    };

    this.separator = {
      type: 'separator',
    };

    this.print = {
      label: 'Imprimir'
    }
  }

  createMenu() {
    let separator1 = {type: this.separator.type};
    let separator2 = {type: this.separator.type};

    _.each({
      view: this.view,
      edit: this.edit,
      separator1: separator1,
      confirm: this.confirm,
      cancel: this.cancel,
      separator2: separator2,
      print: this.print,
      delete: this.delete
    }, (item, k) => {
      if (item.type !== 'separator') {
        item.disabled = true;
        item.icon = null;
        item.select = () => false
      }
      item.hidden = false;

      this.menu[k] = item
    })
  }

  show(menu, target) {
    this.hide();

    if (menu && menu.length) {
      this.menu = menu
    }

    if (typeof this.menu === 'object') {
      let menuList = document.createElement('ol');

      _.each(this.menu, (item) => {
        if (item && !item.hidden) {
          let content = '<span>' + (item.label || '') + '</span>';
          let menuListItem = document.createElement('li');

          if (item.type && item.type === 'separator') {
            menuListItem.classList.add('separator');
            content = '<span></span>'
          }

          menuListItem.addEventListener('click', (e) => {
            if (item.select && !item.disabled) {
              this.hide();
              window.setTimeout(item.select, 150)
            } else if (item.disabled) {
              e.stopPropagation()
            }
          });

          if (item.disabled) {
            menuListItem.classList.add('disabled')
          }

          menuListItem.innerHTML = content;
          menuList.appendChild(menuListItem)
        }
      });

      // Handle context menu boundaries

      this.menuWrapper.innerHTML = '';
      this.menuWrapper.style.display = 'block';
      this.menuWrapper.style.left = this.position.x + 'px';
      this.menuWrapper.style.top = (this.position.y - 5) + 'px';
      this.menuWrapper.appendChild(menuList);
      // document.body.setAttribute('style', 'overflow:hidden')
      document.body.appendChild(this.menuWrapper);

      // Add active class to target element if set
      if (target !== undefined) {
        this.targetEl = target;
        target.classList.add('context-active')
      }
    }
  }

  hide() {
    let ctxEl = _.find(document.body.childNodes, (node) => {
      return node === this.menuWrapper
    });
    if (ctxEl !== undefined) {
      if (this.targetEl !== undefined) {
        document.body.style.overflow = 'auto';
        this.targetEl.classList.remove('context-active')
      }
      document.body.removeChild(this.menuWrapper)
    }
  }

  init() {
    this.menuWrapper = document.createElement('div');
    this.menuWrapper.setAttribute('id', this.menuId);
    this.menuWrapper.setAttribute('class', this.menuClasses);
    this.menuWrapper.style.top = this.position.y;
    this.menuWrapper.style.left = this.position.x;
    this.menuWrapper.onclick = e => e.stopPropagation();
    this.menuWrapper.oncontextmenu = e => e.preventDefault();

    window.addEventListener('mousemove', (e) => {
      this.position.x = e.pageX;
      this.position.y = e.pageY
    });

    this.createMenu();

    window.addEventListener('click', (e) => {
      if (e.button === 0) {
        this.hide()
      }
    })
  }
}

export default ContextMenu;