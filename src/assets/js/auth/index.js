import store from '../store'

import {fetchAuthUser, logout, authenticateUser} from '../helpers'

export default {
  check () {
    return fetchAuthUser()
      .then(res => {
        if (res.status === 200) {
          return res.data
        }
        return null
      })
  },

  login (email, password) {
    return authenticateUser(email, password)
  },

  logout () {
    return logout()
  },

  redirectIfNotAuthenticated (url) {
    if (!store.getters.user) {
      return url !== undefined ? url : '/login'
    }
    return null
  }
}