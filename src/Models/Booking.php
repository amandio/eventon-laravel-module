<?php

namespace CodeonWeekends\Eventon\Models;

use App\User;
use Carbon\Carbon;

class Booking extends BaseModel
{
    public $incrementing = false;

    protected $fillable = [
        'number',
        'contact_full_name',
        'contact_phone',
        'contact_email',
        'contact_document_number',
        'user_id',
        'event_id',
        'ticket_id',
        'transaction_id',
        'status',
        'confirmed_at',
        'cancelled_at',
        'print_at_home',
        'send_to_whatsapp',
        'sent_to_whatsapp_at',
        'print_on_site',
        'printed_at',
        'print_location',
        'print_count',
        'allowed_entrances',
    ];

    protected $casts = [
        'number' => 'string'
    ];

    public function getKeyName()
    {
        return 'number';
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function ticket()
    {
        return $this->hasOne(Ticket::class, 'id', 'ticket_id');
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class, 'id', 'transaction_id');
    }

    public function scopeConfirmed($query)
    {
        return $query->where('status', 1);
    }

    /**
     * @deprecated
     * @param $query
     * @return mixed
     */
    public function scopeUnconfirmed($query)
    {
        return $query->where('status', 0);
    }

    public function scopePending($query)
    {
        return $this->unconfirmed();
    }

    public function scopeCancelled($query)
    {
        return $query->where('status', 2);
    }

    public function scopeBookedToday($query)
    {
        return $query->whereDay('created_at', date('d'));
    }

    public function scopeBookedThisWeek ($query)
    {
        return $query->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
    }

    public function scopeBookedThisMonth ($query)
    {
        return $query->whereMonth('created_at', date('m'));
    }

    public function scopeBookedThisYear ($query)
    {
        return $query->whereYear('created_at', date('Y'));
    }
}