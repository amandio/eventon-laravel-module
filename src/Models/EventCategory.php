<?php

namespace CodeonWeekends\Eventon\Models;

class EventCategory extends BaseModel
{
    protected $table = 'event_categories';
}