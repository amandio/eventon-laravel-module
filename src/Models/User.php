<?php

namespace CodeonWeekends\Eventon\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, SoftDeletes;

    protected $fillable = [
        'email',
        'password',
        'provider',
        'provider_id'
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $with = [
        'profile'
    ];

    public function interests()
    {
        return $this->belongsToMany(Category::class, 'user_interests', 'user_id', 'interest_id');
    }

    public function scopeAdmin()
    {
        return $this->roles()->where('name','admin');
    }

    public function profile ()
    {
        return $this->hasOne(Profile::class);
    }

    public function roles ()
    {
        return $this->belongsToMany(Role::class, 'user_roles');
    }

    public function bookings ()
    {
        return $this->hasMany(Booking::class);
    }

    public function isAdmin ()
    {
        return $this->roles()->where('name','admin')->get()->count();
    }

    public function isPromoter()
    {
        return true; //$this->roles()->where('name','promoter')->get()->count();
    }
}