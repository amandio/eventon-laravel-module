<?php

namespace CodeonWeekends\Eventon\Models;

class Category extends BaseModel
{
    protected $fillable = [
        'name',
        'alias'
    ];

    public function getRouteKeyName()
    {
        return 'alias';
    }

    public function events()
    {
        return $this->belongsToMany(Event::class, 'event_categories');
    }
}
