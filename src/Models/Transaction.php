<?php
/**
 * @copyright 2019.
 * @author Amandio Khuta Nhamande <amandio16@gmail.com>
 * @license MIT
 *
 */

namespace CodeonWeekends\Eventon\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'type',
        'host_ip',
        'host_os',
        'host_user_agent',
        'data',
        'amount',
        'source',
        'description',
        'reference',
        'code',
        'status',
    ];

    protected $connection = 'mysql';
}
