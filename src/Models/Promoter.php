<?php namespace CodeonWeekends\Eventon\Models;

use App\User;
use Illuminate\Support\Facades\DB;

class Promoter extends BaseModel
{
    protected $fillable = [
        'name',
        'alias',
        'branding',
        'phone',
        'phone_alt',
        'email',
        'address',
        'address_city',
        'address_province',
        'representative_name',
        'representative_phone',
        'representative_phone_alt',
        'representative_email',
        'representative_address',
        'representative_address_city',
        'representative_address_province',
        'user_id'
    ];

    protected $hidden = [
        'user_id',
        'created_at',
        'deleted_at',
        'updated_at'
    ];

    protected $with = [
        'user'
    ];

    public function user() {
        return $this->setConnection('mysql')->belongsTo(User::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }

}