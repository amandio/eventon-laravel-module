<?php

namespace CodeonWeekends\Eventon\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Event extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'alias',
        'price',
        'location',
        'date',
        'begin_time',
        'end_time',
        'description',
        'cover',
        'cover_thumbnail',
        'status',
        'location',
        'location_city',
        'promoter_id'
    ];

    protected $with = [
        'categories',
    ];

    protected $casts = [
        'begin_time' => 'datetime',
        'end_time' => 'datetime'
    ];

    protected $appends = [
        'is_available',
        'booking_total',
        'transaction_total',
        'total_bookings',
        'total_confirmed_bookings',
        'total_pending_bookings',
        'total_cancelled_bookings',
        'total_today_bookings',
        'total_week_bookings',
        'total_month_bookings',
        'total_year_bookings'
    ];

    public function setBeginTimeAttribute($value)
    {
        $this->attributes['begin_time'] = Carbon::createFromFormat('d/m/Y H:i', $value);
    }

    public function setEndTimeAttribute($value)
    {
        $this->attributes['end_time'] = Carbon::createFromFormat('d/m/Y H:i', $value);
    }

    public function setAliasAttribute($value)
    {
        $this->attributes['alias'] = Str::slug($value);
    }

    public function getIsAvailableAttribute()
    {
        return (bool) $this->attributes['status'] && $this->begin_time > Carbon::now();
    }

    public function getRouteKeyName()
    {
        return 'alias';
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'event_categories');
    }

    public function tickets()
    {
        return $this->belongsToMany(Ticket::class, 'event_tickets');
    }

    public function promoter()
    {
        return $this->belongsTo(Promoter::class);
    }

    public function scopeFuture ($query)
    {
        return $query->where([
            ['begin_time', '>', Carbon::now()]
        ]);
    }

    public function scopeConfirmed ($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending ($query)
    {
        return $query->where('status', 0);
    }

    public function scopeCity($query, $city)
    {
        return $query->where('location_city', $city);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    /**
     * @deprecated
     * @return mixed
     */
    public function getBookingTotalAttribute()
    {
        return $this->total_bookings;
    }

    public function getTotalBookingsAttribute()
    {
        return $this->bookings()->count();
    }

    public function getTotalConfirmedBookingsAttribute()
    {
        return $this->bookings()->confirmed()->count();
    }

    public function getTotalPendingBookingsAttribute()
    {
        return $this->bookings()->pending()->count();
    }

    public function getTotalCancelledBookingsAttribute()
    {
        return $this->bookings()->cancelled()->count();
    }

    public function getTransactionTotalAttribute()
    {
        $total = 0;

        $bookings = $this->bookings;

        foreach ($bookings as $booking) {
            $total += $booking->transaction->amount;
        }

        return $total;
    }

    public function getTotalTodayBookingsAttribute()
    {
        return $this->bookings()->bookedToday()->count();
    }

    public function getTotalWeekBookingsAttribute()
    {
        return $this->bookings()->bookedThisWeek()->count();
    }

    public function getTotalMonthBookingsAttribute()
    {
        return $this->bookings()->bookedThisMonth()->count();
    }

    public function getTotalYearBookingsAttribute()
    {
        return $this->bookings()->bookedThisYear()->count();
    }
}
