<?php

namespace CodeonWeekends\Eventon\Models;

class UserInterest extends BaseModel
{
    protected $table = 'user_interests';
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'interest_id'
    ];

    public function users()
    {
    }
}