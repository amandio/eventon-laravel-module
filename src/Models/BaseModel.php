<?php

namespace CodeonWeekends\Eventon\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    protected $connection = 'eventon';

    public function scopeUnavailable ($query)
    {
        return $query->where('status', 0);
    }

    public function scopeAvailable ($query)
    {
        return $query->where('status', 1);
    }
}