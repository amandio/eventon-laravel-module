<?php

namespace CodeonWeekends\Eventon\Models;

class Ticket extends BaseModel
{
    protected $fillable = [
        'id',
        'name',
        'description',
        'custom_name',
        'custom_description',
        'quantity',
        'min_reservation_quantity',
        'max_reservation_quantity',
        'price',
        'status'
    ];

    protected $appends = [
        'total_bookings',
        'total_confirmed_bookings',
        'total_cancelled_bookings',
        'total_bookings_amount',
        'event'
    ];

    public function getEventAttribute ()
    {
        return $this->event()->first();
    }

    public function event()
    {
        return $this->belongsToMany(Event::class, 'event_tickets');
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function scopeNotLotated($query)
    {
        return $query->where('is_lotated', false);
    }

    public function scopeLotated($query)
    {
        return $query->where('is_lotated', true);
    }

    public function scopePublished($query)
    {
        return $query->where('status', 1);
    }

    public function scopeUnpublished($query)
    {
        return $query->where('status', 0);
    }

    public function getTotalBookingsAttribute()
    {
        return $this->bookings()->count();
    }

    public function getTotalConfirmedBookingsAttribute()
    {
        return $this->bookings()->confirmed()->count();
    }

    public function getTotalCancelledBookingsAttribute()
    {
        return $this->bookings()->cancelled()->count();
    }

    public function getTotalBookingsAmountAttribute()
    {
        return $this->total_bookings * $this->price;
    }
}