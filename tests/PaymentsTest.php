<?php

namespace CodeonWeekends\Eventon\Tests;

use CodeonWeekends\Eventon\Controllers\PaymentsController;
use PHPUnit\Framework\TestCase;

class PaymentsTest extends TestCase
{
    public function testProcess()
    {
        $amount = 1000;
        $vat = 17; // Equivalent to 17%
        $currency = 'USD';
        $tax = 10.7;
        $v = 0 . '.' .$vat;
        $total_plus_taxes = ceil($amount + ($amount * $v) + $tax);
        $ref = 'EO123MP';

        $payment = new PaymentsController();
        $payment->setVat($vat);

        $result = $payment->process($amount, $currency, $tax, $ref);

        $this->assertInstanceOf(PaymentsController::class, $result);
        $this->assertNotEmpty($result->getTotalAmount());
        $this->assertEquals($total_plus_taxes, $result->getTotalAmount());
        $this->assertNotEmpty($result->getCurrency());
        $this->assertEquals($currency, $result->getCurrency());
        $this->assertEquals($ref, $result->getReference());

        return $result;
    }

    /**
     * @depends testProcess
     * @param PaymentsController $process
     */
    public function testMpesaPayment(PaymentsController $process)
    {
        $process->mpesa(258848914919)->send();
        $response = $process->getResponse()->getBody();

        $this->assertEquals('INS-0', $response->output_ResponseCode);
        $this->assertNotEmpty($response->output_ConversationID);
    }
}